﻿using CavesOfTheDragonAge.Common;
using libtcod;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.UI.UserInterface
{
    public partial class DefaultUserInterface : IUserInterface
    {
        #region Public

        /// <summary>
        /// Display a menu to the user.
        /// </summary>
        /// <param name="header">The prompt text for the menu.</param>
        /// <param name="options">The list of options for the menu.</param>
        /// <param name="forceSelection">If true, will force the user to make a selection.</param>
        /// <param name="width">The width of the menu.</param>
        /// <param name="backgroundImageFilename">If set, the name of the image to display behind the menu.</param>
        /// <returns>Returns the zero-based index of the selection, or -1 if the menu was canceled out of.</returns>
        public int DisplayMenu(string header, List<string> options, bool forceSelection = false, int width = 0, string backgroundImageFilename = null)
        {
            if (options == null)
            {
                options = new List<string>();
            }

            //calculate the width, if needed
            if (width == 0)
            {
                foreach(string line in header.Split(new[] {"\r\n", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries ))
                {
                    if(line.Length > width)
                    {
                        width = line.Length;
                    }
                }

                foreach (string option in options)
                {
                    if (option.Length + 4 > width)
                    {
                        width = option.Length + 4;
                    }
                }
            }
            width++;

            if (width > Con.getWidth())
            {
                width = Con.getWidth();
            }

            //calculate the height of the header
            int headerHeight = 0;
            if (header != "")
            {
                headerHeight = Con.getHeightRect(0, 0, width, Con.getHeight(), header);
            }

            //add one line per option
            int height = headerHeight + options.Count();

            //create an off-screen console that represents the menu's window
            TCODConsole window = new TCODConsole(width, height);
            window.setForegroundColor(ConvertColors.ToTCOD(System.Drawing.Color.White));

            //print the header, with autowrap
            window.printRect(0, 0, width, height, header);

            //print all the options
            int y = headerHeight;
            int letterIndex = 'a';
            for (int i = 0; i < options.Count; i++)
            {
                char letter = (char)letterIndex;
                string text = "(" + letter + ") " + options[i];
                window.print(0, y, text);
                y++;
                letterIndex++;
            }

            //blit the contents of window to Con
            int windowX = Con.getWidth() / 2 - width / 2;
            int windowY = Con.getHeight() / 2 - height / 2;
            if (!string.IsNullOrEmpty(backgroundImageFilename))
            {
                var imageCon = displayImage(backgroundImageFilename);
                TCODConsole.blit(window, 0, 0, window.getWidth(), window.getHeight(), imageCon, windowX, windowY, 1.0f, 0.7f);
                TCODConsole.blit(imageCon, 0, 0, imageCon.getWidth(), imageCon.getHeight(), Con, 0, 0);
            }
            else
            {
                TCODConsole.blit(window, 0, 0, window.getWidth(), window.getHeight(), Con, windowX, windowY, 1.0f, 0.7f);
            }

            // display on the main screen
            TCODConsole.blit(Con, 0, 0, Con.getWidth(), Con.getHeight(), TCODConsole.root, 0, 0);
            TCODConsole.flush();

            while (true)
            {
                //get menu selection
                TCODKey key = waitForKeypress(true);

                int index = key.Character - 'a';
                if (index >= 0 && index < options.Count)
                {
                    return index;
                }
                else if (forceSelection == false)
                {
                    return -1;
                }

                //continue the loop to wait for another keypress
            }
        }

        public string GetTextFromUser(string prompt, string currentText = "", int maxCharacters = 50, int width = 0)
        {
            if (width == 0)
            {
                width = prompt.Length + maxCharacters + 1;
            }

            if (width > Con.getWidth())
            {
                width = Con.getWidth();
            }

            string enteredText = currentText.Length > maxCharacters ? currentText.Substring(0, maxCharacters) : currentText;
            string fullText = prompt + "".PadLeft(maxCharacters) + "_";
            int height = Con.getHeightRect(0, 0, width, Con.getHeight(), fullText);

            //create an off-screen console that represents the text entry window
            TCODConsole window = new TCODConsole(width, height);
            window.setForegroundColor(ConvertColors.ToTCOD(System.Drawing.Color.White));

            while (true)
            {
                fullText = prompt + enteredText + "_";

                //print the current text, with autowrap
                window.clear();
                window.printRect(0, 0, width, height, fullText);

                //blit the contents of window to Con
                int windowX = Con.getWidth() / 2 - width / 2;
                int windowY = Con.getHeight() / 2 - height / 2;
                TCODConsole.blit(window, 0, 0, window.getWidth(), window.getHeight(), Con, windowX, windowY, 1.0f, 1.0f);

                // display on the main screen
                TCODConsole.blit(Con, 0, 0, Con.getWidth(), Con.getHeight(), TCODConsole.root, 0, 0);
                TCODConsole.flush();

                //get next key selection
                TCODKey key = waitForKeypress(true);

                if (key.KeyCode == TCODKeyCode.Enter || key.KeyCode == TCODKeyCode.KeypadEnter)
                {
                    RenderAll(true);
                    return enteredText;
                }
                else if (key.KeyCode == TCODKeyCode.Escape)
                {
                    RenderAll(true);
                    return "";
                }
                else if (key.KeyCode == TCODKeyCode.Backspace && enteredText.Length > 0)
                {
                    enteredText = enteredText.Substring(0, enteredText.Length - 1);
                }
                else if (new List<TCODKeyCode>()
                {
                    TCODKeyCode.Char, TCODKeyCode.One, TCODKeyCode.Two, TCODKeyCode.Three, TCODKeyCode.Four, TCODKeyCode.Five, TCODKeyCode.Six, TCODKeyCode.Seven, TCODKeyCode.Eight,
                    TCODKeyCode.Nine, TCODKeyCode.Zero, TCODKeyCode.Space
                }.Contains(key.KeyCode) && enteredText.Length < maxCharacters)
                {
                    enteredText += key.Character;
                }

                //continue the loop to wait for another keypress
            }
        }

        #endregion Public

        #region Private

        /// <summary>
        /// Returns a <href=Direction>Direction</href> chosen by the user.
        /// </summary>
        /// <returns>A <href=Direction>Direction</href> chosen by the user.</returns>
        private Direction getDirectionFromUser()
        {
            DisplayMessage("Which way?");
            RenderAll();

            Direction direction = Direction.Undefined;

            TCODKey key = waitForKeypress(false);

            //if(key.KeyCode == TCODKeyCode.Escape)
            //{
            //    return Direction.Undefined;
            //}

            direction = interpretKeyDirection(key);

            if (direction == Direction.Undefined)
            {
                return getDirectionFromUser();
            }

            return direction;
        }

        private TCODKey waitForKeypress(bool flush)
        {
            TCODKey key = TCODConsole.waitForKeypress(flush);
            if (TCODConsole.isWindowClosed() == true)
            {
                key = new TCODKey() { KeyCode = TCODKeyCode.Escape };
            }

            //(special case) Alt+Enter: toggle fullscreen
            if (key.KeyCode == TCODKeyCode.Enter && (key.LeftAlt || key.RightAlt))
            {
                TCODConsole.setFullscreen(!TCODConsole.isFullscreen());
                TCODConsole.flush();
                key = waitForKeypress(flush);
            }

            return key;
        }

        private Direction interpretKeyDirection(TCODKey key)
        {
            Direction direction = Direction.Undefined;

            switch (key.KeyCode)
            {
                case TCODKeyCode.KeypadOne:
                case TCODKeyCode.End:
                    direction = Direction.SouthWest;
                    break;

                case TCODKeyCode.KeypadTwo:
                case TCODKeyCode.Down:
                    direction = Direction.South;
                    break;

                case TCODKeyCode.KeypadThree:
                case TCODKeyCode.Pagedown:
                    direction = Direction.SouthEast;
                    break;

                case TCODKeyCode.KeypadFour:
                case TCODKeyCode.Left:
                    direction = Direction.West;
                    break;

                case TCODKeyCode.KeypadFive:
                    direction = Direction.Here;
                    break;

                case TCODKeyCode.KeypadSix:
                case TCODKeyCode.Right:
                    direction = Direction.East;
                    break;

                case TCODKeyCode.KeypadSeven:
                case TCODKeyCode.Home:
                    direction = Direction.NorthWest;
                    break;

                case TCODKeyCode.KeypadEight:
                case TCODKeyCode.Up:
                    direction = Direction.North;
                    break;

                case TCODKeyCode.KeypadNine:
                case TCODKeyCode.Pageup:
                    direction = Direction.NorthEast;
                    break;

                case TCODKeyCode.Char:
                    switch (key.Character)
                    {
                        case 'l':
                            direction = Direction.East;
                            break;
                        case 'k':
                            direction = Direction.North;
                            break;
                        case 'j':
                            direction = Direction.South;
                            break;
                        case 'h':
                            direction = Direction.West;
                            break;
                        case 'y':
                            direction = Direction.NorthWest;
                            break;
                        case 'u':
                            direction = Direction.NorthEast;
                            break;
                        case 'b':
                            direction = Direction.SouthWest;
                            break;
                        case 'n':
                            direction = Direction.SouthEast;
                            break;
                        default:
                            direction = Direction.Undefined;
                            break;
                    }
                    break;

                default:
                    direction = Direction.Undefined;
                    break;
            }

            return direction;
        }

        //private string getTextFromUser(string prompt, int x, int y, int w, int h, int maxCharacters = 50)
        //{
        //    TCODText textField = new TCODText(x, y, w, h, maxCharacters);
        //    textField.setProperties('_', 50, prompt, 4);
        //    textField.setColors(TCODColor.white, TCODColor.black, 1.0f);
        //    textField.render(Con);
        //    TCODConsole.blit(Con, 0, 0, Con.getWidth(), Con.getHeight(), TCODConsole.root, 0, 0);
        //    TCODConsole.flush();

        //    while (true)
        //    {
        //        TCODKey key = waitForKeypress(false);

        //        if (key.KeyCode == TCODKeyCode.Enter || key.KeyCode == TCODKeyCode.KeypadEnter)
        //        {
        //            RenderAll(true);
        //            textField.update(key);
        //            return textField.getText();
        //        }
        //        else if (key.KeyCode == TCODKeyCode.Escape)
        //        {
        //            RenderAll(true);
        //            return "";
        //        }
        //        else
        //        {
        //            textField.update(key);
        //            textField.render(Con);
        //            TCODConsole.blit(Con, 0, 0, Con.getWidth(), Con.getHeight(), TCODConsole.root, 0, 0);
        //            TCODConsole.flush();
        //        }
        //    }
        //}

        /// <summary>
        /// Display the given image, centered on the screen.
        /// </summary>
        /// <param name="filename">The filename of the image to display.</param>
        private TCODConsole displayImage(string filename)
        {
            TCODImage image = new TCODImage(filename);

            int screenWidth = Con.getWidth();
            int screenHeight = Con.getHeight();

            image.scale(screenWidth * 2, screenHeight * 2);

            TCODConsole imageCon = new TCODConsole(screenWidth, screenHeight);

            image.blit2x(imageCon, 0, 0, 0, 0, -1, -1);

            for(int x = 0; x<screenWidth; x++)
            {
                for(int y = 0; y<screenHeight; y++)
                {
                    int chr = imageCon.getChar(x, y);
                    imageCon.setChar(x, y, convertBlockCharacters(chr));
                }
            }

            return imageCon;
        }

        private int convertBlockCharacters(int chr)
        {
            switch (chr)
            {
                case (int)TCODSpecialCharacter.SubpixelNorthWest: return (int)'\u2598';
                    break;
                case (int)TCODSpecialCharacter.SubpixelNorthEast: return (int)'\u259D';
                    break;
                case (int)TCODSpecialCharacter.SubpixelNorth: return (int)'\u2580';
                    break;
                case (int)TCODSpecialCharacter.SubpixelSoutheast: return (int)'\u2597';
                    break;
                case (int)TCODSpecialCharacter.SubpixelDiagonal: return (int)'\u259E';
                    break;
                case (int)TCODSpecialCharacter.SubpixelEast: return (int)'\u2590';
                    break;
                case (int)TCODSpecialCharacter.SubpixelSouthwest: return (int)'\u2596';
                    break;
                default: return chr;
                    break;
            }
        }

        #endregion Private
    }
}
