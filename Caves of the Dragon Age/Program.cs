﻿using System;

namespace CavesOfTheDragonAge
{
    /// <summary>
    /// program
    /// </summary>
    internal static class Program
    {
        /// <summary>
        /// main
        /// </summary>
        private static void Main()
        {
            //ResourceExtractor.ExtractAllResourceFiles();

            //var userInterfaceType = Type.GetType("CavesOfTheDragonAge.UI.UserInterface.UserInterfaceFactory, CavesOfTheDragonAge.UI", true);
            //var userInterface = userInterfaceType.GetMethod("GetUserInterface").Invoke(null, new object[] { "default" });

            //var gameType = Type.GetType("CavesOfTheDragonAge.Engine.Game, CavesOfTheDragonAge.Engine", true);
            //dynamic game = Activator.CreateInstance(gameType, userInterface);
            //game.Run();

            Common.IUserInterface userInterface = UI.UserInterface.UserInterfaceFactory.GetUserInterface();
            Engine.Game game = new Engine.Game(userInterface);
            game.MainMenu();
        }
    }
}