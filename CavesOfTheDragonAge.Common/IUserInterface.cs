﻿using System.Collections.Generic;

namespace CavesOfTheDragonAge.Common
{
    public interface IUserInterface
    {
        void DisplayMessage(string message);

        void DisplayMessage(string message, System.Drawing.Color color);

        CreatureAction GetPlayerAction();

        void InitUi(int width = 80, int height = 50, string title = "Caves of the Dragon Age");

        void RenderAll(bool keepMessages = false);

        string PlayerId { get; set; }

        bool UserCommandWaiting { get; }

        int DisplayMenu(string header, List<string> options, bool forceSelection = false, int width = 0, string backgroundImageFilename = null);

        string GetTextFromUser(string prompt, string currentText = "", int maxCharacters = 50, int width = 0);
    }
}