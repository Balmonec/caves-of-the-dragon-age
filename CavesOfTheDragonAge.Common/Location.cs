﻿using System;

namespace CavesOfTheDragonAge.Common
{
    /// <summary>
    /// A pair of coordinates indicating a location on the map
    /// </summary>
    public class Location : Tuple<int, int>
    {
        #region [-- Public Methods --]

        /// <summary>
        /// Construct a new Location.
        /// </summary>
        /// <param name="x">The X coordinate of the location.</param>
        /// <param name="y">The Y coordinate of the location.</param>
        public Location(int x, int y)
            : base(x, y)
        {
            //Do nothing, just the base constructor will do.
        }

        /// <summary>
        /// Construct a new Location.
        /// </summary>
        /// <param name="location">A Tuple containing the (x, y) coordinates of the location.</param>
        public Location(Tuple<int, int> location) : this(location.Item1, location.Item2)
        {

        }

        /// <summary>
        /// Overload the subtraction operator for the Location type.
        /// </summary>
        /// <param name="left">The Location on the left side of the operator.</param>
        /// <param name="right">The Location on the right side of the operator.</param>
        /// <returns>The Location that is the result of the operator</returns>
        public static Location operator -(Location left, Location right)
        {
            return new Location(left.X - right.X, left.Y - right.Y);
        }

        /// <summary>
        /// Overload the addition operator for the Location type.
        /// </summary>
        /// <param name="left">The Location on the left side of the operator.</param>
        /// <param name="right">The Location on the right side of the operator.</param>
        /// <returns>The Location that is the result of the operator</returns>
        public static Location operator +(Location left, Location right)
        {
            return new Location(left.X + right.X, left.Y + right.Y);
        }

        /// <summary>
        /// Overload the equals operator for the Location type.
        /// </summary>
        /// <param name="left">The Location on the left side of the operator.</param>
        /// <param name="right">The Location on the right side of the operator.</param>
        /// <returns>The Boolean that is the result of the operator.</returns>
        public static bool operator ==(Location left, Location right)
        {
            if ((Tuple<int, int>)left == null && (Tuple<int, int>)right == null) return true;
            if ((Tuple<int, int>) left == null || (Tuple<int, int>) right == null) return false;
            return left.X == right.X && left.Y == right.Y;
        }

        /// <summary>
        /// Overload the not-equals operator for the Location type.
        /// </summary>
        /// <param name="left">The Location on the left side of the operator.</param>
        /// <param name="right">The Location on the right side of the operator.</param>
        /// <returns>The Boolean that is the result of the operator.</returns>
        public static bool operator !=(Location left, Location right)
        {
            return !(left == right);
        }


        /// <summary>
        /// Gets the location of the adjacent cell in the given direction.
        /// </summary>
        /// <param name="direction">The direction of the adjacent cell.</param>
        /// <returns>A Location indicating the adjacent cell in the given direction.</returns>
        public Location GetVector(Direction direction)
        {
            int newX = X + direction.GetDeltaX();
            int newY = Y + direction.GetDeltaY();

            return new Location(newX, newY);
        }

        /// <summary>
        /// Get the closest corresponding direction pointing towards the given location from the current location.
        /// If it isn't in a straight line, it rounds clockwise.
        /// </summary>
        /// <param name="newLocation">The location to point towards.</param>
        /// <returns>The closest possible Direction to the straight line towards the new location</returns>
        public Direction GetDirectionTowardsClockwise(Location newLocation)
        {
            if (this == newLocation || newLocation == null)
            {
                return Direction.Undefined;
            }
            else
            {
                return GetDirectionTowardsClockwise(newLocation.X, newLocation.Y);
            }
        }

        /// <summary>
        /// Get the closest corresponding direction pointing towards the given location from the current location.
        /// If it isn't in a straight line, it rounds clockwise.
        /// </summary>
        /// <param name="x">The X-coordinate to point towards.</param>
        /// <param name="y">The y-coordinate to point towards.</param>
        /// <returns>The closest possible Direction to the straight line towards the new location</returns>
        public Direction GetDirectionTowardsClockwise(int x, int y)
        {
            if (x == this.X && y == this.Y)
            {
                return Direction.Undefined;
            }

            double atan2 = (Math.Atan2(y - this.Y, x - this.X)) / Math.PI;

            int number = (int)Math.Ceiling((atan2 + 1) * 4);

            switch (number % 8)
            {
                case 0:
                    return Direction.West;

                case 1:
                    return Direction.NorthWest;

                case 2:
                    return Direction.North;

                case 3:
                    return Direction.NorthEast;

                case 4:
                    return Direction.East;

                case 5:
                    return Direction.SouthEast;

                case 6:
                    return Direction.South;

                case 7:
                    return Direction.SouthWest;

                default:
                    return Direction.Undefined;
            }
        }

        /// <summary>
        /// Get the closest corresponding direction pointing towards the given location from the current location.
        /// If it isn't in a straight line, it rounds counterclockwise.
        /// </summary>
        /// <param name="newLocation">The location to point towards.</param>
        /// <returns>The closest possible Direction to the straight line towards the new location</returns>
        public Direction GetDirectionTowardsCounterclockwise(Location newLocation)
        {
            if (this == newLocation || newLocation == null)
            {
                return Direction.Undefined;
            }
            else
            {
                return GetDirectionTowardsCounterclockwise(newLocation.X, newLocation.Y);
            }
        }

        /// <summary>
        /// Get the closest corresponding direction pointing towards the given location from the current location.
        /// If it isn't in a straight line, it rounds counterclockwise.
        /// </summary>
        /// <param name="x">The X-coordinate to point towards.</param>
        /// <param name="y">The y-coordinate to point towards.</param>
        /// <returns>The closest possible Direction to the straight line towards the new location</returns>
        public Direction GetDirectionTowardsCounterclockwise(int x, int y)
        {
            if (x == this.X && y == this.Y)
            {
                return Direction.Undefined;
            }

            double atan2 = (Math.Atan2(y - this.Y, x - this.X)) / Math.PI;

            int number = (int)Math.Floor((atan2 + 1) * 4);

            switch (number % 8)
            {
                case 0:
                    return Direction.West;

                case 1:
                    return Direction.NorthWest;

                case 2:
                    return Direction.North;

                case 3:
                    return Direction.NorthEast;

                case 4:
                    return Direction.East;

                case 5:
                    return Direction.SouthEast;

                case 6:
                    return Direction.South;

                case 7:
                    return Direction.SouthWest;

                default:
                    return Direction.Undefined;
            }
        }

        #endregion [-- Public Methods --]

        #region [-- Properties --]

        /// <summary>
        /// The X coordinate of the Location.
        /// </summary>
        public int X
        {
            get { return Item1; }
        }

        /// <summary>
        /// The Y coordinate of the Location.
        /// </summary>
        public int Y
        {
            get { return Item2; }
        }

        #endregion [-- Properties --]
    }
}