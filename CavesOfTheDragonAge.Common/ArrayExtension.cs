﻿using System;
namespace CavesOfTheDragonAge.Common
{
    /// <summary>
    /// An extension class for Arrays.
    /// </summary>
    public static class ArrayExtension
    {
        /// <summary>
        /// Determine if the given x and y are within the bounds of the array.
        /// </summary>
        /// <param name="array">The current two-dimensional array object to check.</param>
        /// <param name="x">An integer indicating the first index to check.</param>
        /// <param name="y">An integer indicating the second index to check.</param>
        /// <returns>Returns true if the indexes are within the bounds, and false otherwise.</returns>
        public static bool IndexInBounds2D(this Array array, int x, int y)
        {
            return array.IndexInBounds2D(new Location(x, y));
        }

        /// <summary>
        /// Determine if the given location is within the bounds of the array.
        /// </summary>
        /// <param name="array">The current two-dimensional array object to check.</param>
        /// <param name="location">A Location to check.</param>
        /// <returns>Returns true if the location is within the bounds, and false otherwise.</returns>
        public static bool IndexInBounds2D(this Array array, Location location)
        {
            if (array.Rank != 2)
            {
                return false;
            }

            return array.IndexInBounds2D(location, array.GetUpperBound(0), array.GetUpperBound(1));
        }

        /// <summary>
        /// Determine if the given location is within the bounds of the array.
        /// </summary>
        /// <param name="array">The current two-dimensional array object to check.</param>
        /// <param name="location">A Location to check.</param>
        /// <returns>Returns true if the location is within the bounds, and false otherwise.</returns>
        public static bool IndexInBounds2D(this Array array, Location location, int xUpperBound, int yLowerBound)
        {
            if (array.Rank != 2)
            {
                return false;
            }

            if (location.X < 0 ||
                location.X > xUpperBound ||
                location.Y < 0 ||
                location.Y > yLowerBound
                )
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Set all elements of the one-dimensional array to the given default value.
        /// </summary>
        /// <typeparam name="T">The type of the elements in the array.</typeparam>
        /// <param name="array">The current one-dimensional array object to modify.</param>
        /// <param name="defaultValue">The value to set as the default.</param>
        public static void Init1D<T>(this T[] array, T defaultValue)
        {
            if(array == null)
            {
                return;
            }

            for(int i = 0; i < array.Length; i++)
            {
                array[i] = defaultValue;
            }
        }

        /// <summary>
        /// Set all elements of the two-dimensional array to the given default value.
        /// </summary>
        /// <typeparam name="T">The type of the elements in the array.</typeparam>
        /// <param name="array">The current two-dimensional array object to modify.</param>
        /// <param name="defaultValue">The value to set as the default.</param>
        public static void Init2D<T>(this T[,] array, T defaultValue)
        {
            if (array == null)
            {
                return;
            }

            for (int x = 0; x < array.GetLength(0); x++)
            {
                for (int y = 0; y < array.GetLength(1); y++)
                {
                    array[x, y] = defaultValue;
                }
            }
        }
    }
}
