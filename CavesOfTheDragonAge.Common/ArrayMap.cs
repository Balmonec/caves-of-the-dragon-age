﻿using System;

namespace CavesOfTheDragonAge.Common
{
    public class ArrayMap
    {
        public ArrayMap(int xSize, int ySize)
        {
            _innerMapArray = new int[xSize, ySize];
            _innerMapArray.Init2D(int.MaxValue);
        }

        public ArrayMap(Tuple<int, int> size) : this(size.Item1, size.Item2)
        {
        }

        public int this[int x, int y]
        {
            get
            {
                return _innerMapArray[x, y];
            }
            set
            {
                _innerMapArray[x, y] = value;
            }
        }

        /// <summary>
        /// The Size of the Dijkstra Map (width, height).
        /// </summary>
        public Tuple<int, int> Size
        {
            get
            {
                return new Tuple<int, int>(XLength, YLength);
            }
        }

        public int XLength
        {
            get { return (_xLength ?? (_xLength = _innerMapArray.GetLength(0))).Value; }
        }

        public int YLength
        {
            get { return (_yLength ?? (_yLength = _innerMapArray.GetLength(1))).Value; }
        }

        public void Reset()
        {
            _innerMapArray.Init2D(int.MaxValue);
        }

        public bool IsOutOfBounds(Location location)
        {
            return !_innerMapArray.IndexInBounds2D(location, XLength - 1, YLength - 1);
        }

        public bool DoesPathExists(Location location)
        {
            return _innerMapArray[location.X, location.Y] == int.MaxValue;
        }

        private int? _xLength;
        private int? _yLength;
        private int[,] _innerMapArray;
    }
}
