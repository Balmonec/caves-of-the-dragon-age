﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace CavesOfTheDragonAge.Common
{
    public static class GenericEnumExtensions
    {
        /// <summary>
        /// Return the description of the enum value.
        /// </summary>
        /// <typeparam name="T">The enum being used.</typeparam>
        /// <param name="enumValue">The enum value to describe.</param>
        /// <returns>The description of the enum value.</returns>
        public static string GetDescription<T>(this T enumValue) where T : struct
        {
            Type type = enumValue.GetType();
            if (!type.IsEnum)
            {
                throw new ArgumentException(enumValue.ToString() + " must be an enumerated value.", "enumValue");
            }

            MemberInfo[] memberInfo = type.GetMember(enumValue.ToString());

            if (memberInfo.Length > 0)
            {
                var attributes = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes.Length > 0)
                {
                    return ((DescriptionAttribute)attributes[0]).Description;
                }
            }

            return enumValue.ToString();
        }
    }
}
