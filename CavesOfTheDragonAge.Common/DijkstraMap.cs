﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CavesOfTheDragonAge.Common
{

    /// <summary>
    /// An integer array containing the distance from any point to the nearest goal cell.
    /// </summary>
    public class DijkstraMap
    {
        #region [-- Constructors --]

        /// <summary>
        /// Create a new DijkstraMap of the given size.
        /// </summary>
        /// <param name="width">The width of the Dijkstra Map.</param>
        /// <param name="height">The height of the Dijkstra Map.</param>
        public DijkstraMap(int width, int height) : this(new Tuple<int, int>(width, height)) { }

        /// <summary>
        /// Create a new DijkstraMap of the given size.
        /// </summary>
        /// <param name="size">The Size of the Dijkstra Map (width, height).</param>
        public DijkstraMap(Tuple<int, int> size)
        {
            _goalMap = new ArrayMap(size);
            _fleeMap = new ArrayMap(size);

            //GoalCells = new bool[size.Item1, size.Item2];
            //ImpassableCells = new bool[size.Item1, size.Item2];
        }

        #endregion [-- Constructors --]

        #region [-- Public Methods --]

        /// <summary>
        /// Build the map based on the Goal Cells and Impassable Cells.
        /// </summary>
        /// <param name="goalCells">The cells to be marked as goals, where True is a goal cell and False is not.  Must be the same size as the map.</param>
        /// <param name="impassableCells">The cells to be marked as impassable (and ignored in the map creation).  Must be the same size as the map.</param>
        public void BuildMap(bool[,] goalCells, bool[,] impassableCells = null)
        {
            #region Validate Parameters

            if(goalCells == null)
            {
                throw new ArgumentNullException("goalCells");
            }

            if(impassableCells == null)
            {
                impassableCells = new bool[Size.Item1, Size.Item2];
            }

            if (goalCells.GetLength(0) != Size.Item1 || goalCells.GetLength(1) != Size.Item2)
            {
                throw new ArgumentOutOfRangeException("goalCells", "The goalCells array must match the size of the map itself.");
            }

            if (impassableCells.GetLength(0) != Size.Item1 || impassableCells.GetLength(1) != Size.Item2)
            {
                throw new ArgumentOutOfRangeException("impassableCells", "The impassableCells array must match the size of the map itself.");
            }

            #endregion Validate Parameters

            _goalMap.Reset();

            for (int x = 0; x < Size.Item1; x++)
            {
                for (int y = 0; y < Size.Item2; y++)
                {
                    if(goalCells[x, y])
                    {
                        _goalMap[x, y] = 0;
                    }
                }
            }

            iterateMapNumbersByLocationList(_goalMap, impassableCells);

            for (int x = 0; x < Size.Item1; x++)
            {
                for (int y = 0; y < Size.Item2; y++)
                {
                    if (!impassableCells[x, y])
                    {
                        _fleeMap[x, y] = (int)(_goalMap[x, y] * -1.2);
                    }
                }
            }

            iterateMapNumbersByLocationList(_fleeMap, impassableCells);
        }

        /// <summary>
        /// Get the direction to be moved for this map, based on whether movement should be towards or away from the goal cells.
        /// </summary>
        /// <param name="location">The Location on the map.</param>
        /// <param name="desire">
        /// An integer indicating how much the goal is desired.
        /// Positive numbers indicate a desire for the goal cells,
        /// while Negative numbers indicate a desire to avoid the
        /// goal cells, and 0 indicates indifference.
        /// </param>
        /// <returns>The Direction to be moved.</returns>
        public Direction GetDirection(Location location, int desire = 1)
        {
            #region Parameter Validation

            if (_goalMap.IsOutOfBounds(location))
            {
                throw new ArgumentOutOfRangeException("location", "The location must be within the bounds of the map.");
            }

            #endregion Parameter Validation

            if (_goalMap.DoesPathExists(location))
            {
                //Either the cell is impassable, or no path exists.
                return Direction.Here;
            }

            List<Direction> bestDirections = new List<Direction>();
            int lowestValue = int.MaxValue;

            foreach (Direction direction in new List<Direction> { Direction.Here, Direction.North, Direction.South, Direction.East, Direction.West, Direction.NorthEast, Direction.NorthWest, Direction.SouthEast, Direction.SouthWest })
            {
                Location newLocation = location.GetVector(direction);
                if (_goalMap.IsOutOfBounds(newLocation))
                {
                    continue;
                }

                if (_goalMap.DoesPathExists(newLocation))
                {
                    //Either the cell is impassable, or no path exists.
                    continue;
                }
                if (desire >= 0)
                {
                    if (_goalMap[newLocation.X, newLocation.Y] * desire < lowestValue)
                    {
                        bestDirections.Clear();
                        bestDirections.Add(direction);
                        lowestValue = _goalMap[newLocation.X, newLocation.Y] * desire;
                    }
                    else if (_goalMap[newLocation.X, newLocation.Y] * desire == lowestValue)
                    {
                        bestDirections.Add(direction);
                    }
                }
                else
                {
                    if (_fleeMap[newLocation.X, newLocation.Y] * Math.Abs(desire) < lowestValue)
                    {
                        bestDirections.Clear();
                        bestDirections.Add(direction);
                        lowestValue = _fleeMap[newLocation.X, newLocation.Y] * Math.Abs(desire);
                    }
                    else if (_fleeMap[newLocation.X, newLocation.Y] * Math.Abs(desire) == lowestValue)
                    {
                        bestDirections.Add(direction);
                    }

                }
            }

            if (bestDirections.Count == 1)
            {
                return bestDirections[0];
            }
            else if (bestDirections.Count > 1)
            {
                return bestDirections[Math.Abs(lowestValue % bestDirections.Count)];
            }
            else
            {
                return Direction.Here;
            }
        }

        /// <summary>
        /// The Dijkstra map towards the goal cells.
        /// </summary>
        public int GoalMap(int x, int y)
        {
            return _goalMap[x, y];
        }

        /// <summary>
        /// The Dijkstra map away from the goal cells.
        /// </summary>
        public int FleeMap(int x, int y)
        {
            return _fleeMap[x, y];
        }

        #endregion [-- Public Methods --]

        #region [-- Private Methods --]
        
        private static void iterateMapNumbersByFullMap(ArrayMap map, bool[,] impassableCells)
        {
            bool changed = true;
            Location currentLocation;
            Location directionLocation;

            while (changed)
            {
                changed = false;

                for (int x = 0; x < map.XLength; x++)
                {
                    for (int y = 0; y < map.YLength; y++)
                    {
                        if (impassableCells[x, y])
                        {
                            continue;
                        }

                        currentLocation = new Location(x, y);
                        directionLocation = new Location(x, y);

                        foreach(Direction direction in new List<Direction> {Direction.North, Direction.South, Direction.East, Direction.West })
                        {
                            directionLocation = currentLocation.GetVector(direction);

                            if (map.IsOutOfBounds(directionLocation))
                                //|| impassableCells[directionLocation.X, directionLocation.Y] == true)
                            {
                                continue;
                            }

                            if (map[currentLocation.X, currentLocation.Y] > map[directionLocation.X, directionLocation.Y] + orthogonalCost && map[directionLocation.X, directionLocation.Y] != int.MaxValue)
                            {
                                map[currentLocation.X, currentLocation.Y] = map[directionLocation.X, directionLocation.Y] + orthogonalCost;
                                changed = true;
                            }
                        }

                        foreach (Direction direction in new List<Direction> { Direction.NorthEast, Direction.NorthWest, Direction.SouthEast, Direction.SouthWest })
                        {
                            directionLocation = currentLocation.GetVector(direction);

                            if (map.IsOutOfBounds(directionLocation))
                                //|| impassableCells[directionLocation.X, directionLocation.Y] == true)
                            {
                                continue;
                            }

                            if (map[currentLocation.X, currentLocation.Y] > map[directionLocation.X, directionLocation.Y] + diagonalCost && map[directionLocation.X, directionLocation.Y] != int.MaxValue)
                            {
                                map[currentLocation.X, currentLocation.Y] = map[directionLocation.X, directionLocation.Y] + diagonalCost;
                                changed = true;
                            }
                        }
                    }
                }
            }
        }

        private static void iterateMapNumbersByLocationList(ArrayMap map, bool[,] impassableCells)
        {
            List<Location> locationsToCheck = new List<Location>();
            Location currentLocation;
            Location directionLocation;
            int directionCost;
            List<Direction> horizontalDirections = Enum.GetValues(typeof(Direction))
                                                       .Cast<Direction>()
                                                       .Where(dir => dir.IsHorizontalMovement())
                                                       .ToList();

            for (int x = 0; x < map.XLength; x++)
            {
                for (int y = 0; y < map.YLength; y++)
                {
                    if (impassableCells[x, y] == true || map[x, y] == int.MaxValue)
                    {
                        continue;
                    }

                    currentLocation = new Location(x, y);

                    foreach (Direction direction in horizontalDirections)
                    {
                        directionLocation = currentLocation.GetVector(direction);

                        if (map.IsOutOfBounds(directionLocation))
                        {
                            continue;
                        }

                        directionCost = direction.IsOrthogonalMovement() ? orthogonalCost : diagonalCost;

                        if (map[directionLocation.X, directionLocation.Y] > map[currentLocation.X, currentLocation.Y] + directionCost)
                        {
                            locationsToCheck.AddRange(horizontalDirections
                                                        .Select(dir => currentLocation.GetVector(dir))
                                                        .Where(loc => !map.IsOutOfBounds(loc)));
                            break;
                        }
                    }
                }
            }

            for (int i = 0; i < locationsToCheck.Count; i++)
            {
                currentLocation = locationsToCheck[i];

                if (map.IsOutOfBounds(currentLocation) || impassableCells[currentLocation.X, currentLocation.Y] == true)
                {
                    continue;
                }

                foreach (Direction direction in horizontalDirections)
                {
                    directionLocation = currentLocation.GetVector(direction);

                    if (map.IsOutOfBounds(directionLocation) || map[directionLocation.X, directionLocation.Y] == int.MaxValue)
                    {
                        continue;
                    }

                    directionCost = direction.IsOrthogonalMovement() ? orthogonalCost : diagonalCost;

                    if (map[currentLocation.X, currentLocation.Y] > map[directionLocation.X, directionLocation.Y] + directionCost)
                    {
                        map[currentLocation.X, currentLocation.Y] = map[directionLocation.X, directionLocation.Y] + directionCost;

                        locationsToCheck.AddRange(horizontalDirections
                                                    .Select(dir => currentLocation.GetVector(dir))
                                                    .Where(loc => !map.IsOutOfBounds(loc)));
                    }
                }
            }
        }

        #endregion [-- Private Methods --]

        #region [-- Properties --]

        /// <summary>
        /// The Size of the Dijkstra Map (width, height).
        /// </summary>
        public Tuple<int, int> Size
        {
            get
            {
                return _goalMap.Size;
            }
        }

        /////// <summary>
        /////// The cells to be marked as goals, where True is a goal cell and False is not.
        /////// </summary>
        ////public bool[,] GoalCells { get; set; }

        /////// <summary>
        /////// The cells to be marked as impassable (and ignored in the map creation).
        /////// </summary>
        ////public bool[,] ImpassableCells { get; set; }

        #endregion [-- Properties --]

        #region [-- Fields --]
        private readonly ArrayMap _goalMap;
        private readonly ArrayMap _fleeMap;

        private const int orthogonalCost = 10;
        private const int diagonalCost = 15;
        #endregion [-- Fields --]
    }
}
