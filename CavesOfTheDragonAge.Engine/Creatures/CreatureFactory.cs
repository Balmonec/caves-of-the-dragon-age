﻿using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine.Creatures.AI;
using CavesOfTheDragonAge.Engine.MagicSpells;
using System.Collections.Generic;

namespace CavesOfTheDragonAge.Engine.Creatures
{
    /// <summary>
    /// A factory class for the Creature object, containing various creature generation functions.
    /// </summary>
    public static partial class CreatureFactory
    {
        /// <summary>
        /// Generates a Creature for a new player.
        /// </summary>
        /// <returns>A Creature for the new player.</returns>
        public static Creature GetNewPlayer()
        {
            Creature player = new Creature
            {
                Identifier = GameData.Ui.PlayerId,
                Name = "Player",
                Appearance = new DisplayCharacter('@', System.Drawing.Color.White),
                Ai = new PlayerAi(),
                Health = new StatPoint(10),
                SpellsKnown = new List<MagicSpell>()
            };

            //player.SpellsKnown.Add(MagicSpellFactory.GetNewMagicSpell("Self Heal", MagicRuneFactory.GetNewTargetRune(TargetType.Self), MagicRuneFactory.GetNewAreaRune("SINGLE"), MagicRuneFactory.GetNewEffectRune("HEAL")));
            //player.SpellsKnown.Add(MagicSpellFactory.GetNewMagicSpell("Fireball", MagicRuneFactory.GetNewTargetRune(TargetType.LineOfSight), MagicRuneFactory.GetNewAreaRune("SMALLEXPLOSION"), MagicRuneFactory.GetNewEffectRune("FIRE")));
            //player.SpellsKnown.Add(MagicSpellFactory.GetNewMagicSpell("Ice Vortex", MagicRuneFactory.GetNewTargetRune(TargetType.Self), MagicRuneFactory.GetNewAreaRune("MEDIUMEXPLOSION"), MagicRuneFactory.GetNewEffectRune("ICE")));
            //player.SpellsKnown.Add(MagicSpellFactory.GetNewMagicSpell("Blink", MagicRuneFactory.GetNewTargetRune(TargetType.Adjacent), MagicRuneFactory.GetNewAreaRune("HUGECONE"), MagicRuneFactory.GetNewEffectRune("TELEPORT")));
            //player.SpellsKnown.Add(MagicSpellFactory.GetNewMagicSpell("Energy Wave", MagicRuneFactory.GetNewTargetRune(TargetType.LineOfSight), MagicRuneFactory.GetNewAreaRune("MEDIUMCONE"), MagicRuneFactory.GetNewEffectRune("FORCE")));
            //player.SpellsKnown.Add(MagicSpellFactory.GetNewMagicSpell("Lightning Bolt", MagicRuneFactory.GetNewTargetRune(TargetType.Adjacent), MagicRuneFactory.GetNewAreaRune("LONGLINE"), MagicRuneFactory.GetNewEffectRune("LIGHTNING")));
            //player.SpellsKnown.Add(MagicSpellFactory.GetNewMagicSpell("Level Buster", MagicRuneFactory.GetNewTargetRune(TargetType.Self), MagicRuneFactory.GetNewAreaRune("LEVEL"), MagicRuneFactory.GetNewEffectRune("FIRE")));

            player.Ai.MyCreature = player;


            return player;
        }

        /// <summary>
        /// Generates a Creature based on the creatureType.
        /// </summary>
        /// <param name="creatureType">The type of creature to make.</param>
        /// <param name="idNum">The unique ID of the creature to make.</param>
        /// <returns>A new Creature based on the type.</returns>
        public static Creature GetNewCreature(string creatureType, int idNum)
        {
            Creature newCreature;

            switch (creatureType)
            {
                case "PLAYER":
                    newCreature = GetNewPlayer();
                    break;

                case "ORC":
                    newCreature = new Creature
                    {
                        Identifier = "ORC" + idNum,
                        Name = "Orc",
                        Appearance = new DisplayCharacter('o', System.Drawing.Color.Green),
                        Ai = new AI.Decorators.AttackerAIDecorator(new DijkstraPathToPlayerAI()),
                        Health = new StatPoint(10)
                    };
                    newCreature.Ai.MyCreature = newCreature;
                    break;

                case "GOBLIN":
                    newCreature = new Creature
                    {
                        Identifier = "GOBLIN" + idNum,
                        Name = "Goblin",
                        Appearance = new DisplayCharacter('g', System.Drawing.Color.Green),
                        Ai = new AI.Decorators.AttackerAIDecorator(new DijkstraPathToPlayerAI()),
                        Health = new StatPoint(5)
                    };
                    newCreature.Ai.MyCreature = newCreature;
                    break;

                case "KOBOLD":
                    newCreature = new Creature
                    {
                        Identifier = "KOBOLD" + idNum,
                        Name = "Kobold",
                        Appearance = new DisplayCharacter('k', System.Drawing.Color.OrangeRed),
                        Ai = new AI.Decorators.AttackerAIDecorator(new DijkstraPathToPlayerAI()),
                        Health = new StatPoint(3)
                    };
                    newCreature.Ai.MyCreature = newCreature;
                    break;

                case "FIREELEMENTAL":
                    newCreature = new Creature
                    {
                        Identifier = "FIREELEMENTAL" + idNum,
                        Name = "Fire Elemental",
                        Appearance = new DisplayCharacter('E', System.Drawing.Color.Red),
                        Ai = new AI.Decorators.AttackerAIDecorator(new DijkstraPathToPlayerAI()),
                        Health = new StatPoint(20)
                    };
                    newCreature.Ai.MyCreature = newCreature;
                    break;

                case "WATERELEMENTAL":
                    newCreature = new Creature
                    {
                        Identifier = "WATERELEMENTAL" + idNum,
                        Name = "Water Elemental",
                        Appearance = new DisplayCharacter('E', GameColors.Azure),
                        Ai = new AI.Decorators.AttackerAIDecorator(new DijkstraPathToPlayerAI()),
                        Health = new StatPoint(20)
                    };
                    newCreature.Ai.MyCreature = newCreature;
                    break;

                case "AIRELEMENTAL":
                    newCreature = new Creature
                    {
                        Identifier = "AIRELEMENTAL" + idNum,
                        Name = "Air Elemental",
                        Appearance = new DisplayCharacter('E', System.Drawing.Color.White),
                        Ai = new AI.Decorators.AttackerAIDecorator(new DijkstraPathToPlayerAI()),
                        Health = new StatPoint(20)
                    };
                    newCreature.Ai.MyCreature = newCreature;
                    break;

                case "EARTHELEMENTAL":
                    newCreature = new Creature
                    {
                        Identifier = "EARTHELEMENTAL" + idNum,
                        Name = "Earth Elemental",
                        Appearance = new DisplayCharacter('E', GameColors.Brown),
                        Ai = new AI.Decorators.AttackerAIDecorator(new DijkstraPathToPlayerAI()),
                        Health = new StatPoint(20)
                    };
                    newCreature.Ai.MyCreature = newCreature;
                    break;

                case "ARCANEANOMALY":
                default:
                    newCreature = new Creature
                    {
                        Identifier = "ARCANEANOMALY" + idNum,
                        Name = "Arcane Anomaly",
                        Appearance = new DisplayCharacter('A', GameColors.BlueViolet),
                        Ai = new AI.Decorators.AttackerAIDecorator(new BasicAI()),
                        Health = new StatPoint(1)
                    };
                    newCreature.Ai.MyCreature = newCreature;
                    break;
            }

            return newCreature;
        }
    }
}