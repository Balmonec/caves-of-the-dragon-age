﻿using CavesOfTheDragonAge.Common;

namespace CavesOfTheDragonAge.Engine.Creatures.AI
{
    class BasicAI : ICreatureAI
    {
        #region [-- Public Methods --]

        /// <summary>
        /// Determine the next action for the creature to take.
        /// </summary>
        /// <returns>
        /// A CreatureAction containing the instructions for the
        /// next action for the creature to take.
        /// </returns>
        public CreatureAction GetNextAction()
        {
            CreatureAction action = new CreatureAction(CreatureActionType.Move, (Direction)Randomization.RandomInt(2, 9));

            return action;
        }

        #endregion [-- Public Methods --]

        #region [-- Private Methods --]

        #endregion [-- Private Methods --]

        #region [-- Properties --]

        /// <summary>
        /// The creature this AI is part of.
        /// </summary>
        public Creature MyCreature { get; set; }

        #endregion [-- Properties --]
    }
}
