﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CavesOfTheDragonAge.Common;

namespace CavesOfTheDragonAge.Engine.Creatures.AI.Decorators
{
    class AttackerAIDecorator : AIDecorator
    {
        public AttackerAIDecorator(ICreatureAI creatureAI) : base(creatureAI)
        {
        }

        public override CreatureAction GetNextAction()
        {
            CreatureAction action = base.GetNextAction();

            if(action.ActionType == CreatureActionType.Move)
            {
                Location myLocation = GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(MyCreature.Identifier);
                Location targetLocation = myLocation.GetVector(action.ActionDirection);
                Creature targetCreature = GameData.DungeonMaps[GameData.CurrentDungeon].GetCreature(targetLocation);

                if(targetCreature != null && targetCreature.Identifier != MyCreature.Identifier)
                {
                    action.ActionType = CreatureActionType.Attack;
                }
            }

            return action;
        }
    }
}
