﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CavesOfTheDragonAge.Common;

namespace CavesOfTheDragonAge.Engine.Creatures.AI.Decorators
{
    class AIDecorator : ICreatureAI
    {
        protected ICreatureAI baseCreatureAI = null;

        public AIDecorator(ICreatureAI creatureAI)
        {
            baseCreatureAI = creatureAI;
        }

        public virtual CreatureAction GetNextAction()
        {
            return baseCreatureAI.GetNextAction();
        }

        /// <summary>
        /// The creature this AI is part of.
        /// </summary>
        public Creature MyCreature
        {
            get { return baseCreatureAI.MyCreature; }
            set { baseCreatureAI.MyCreature = value; }
        }
    }
}
