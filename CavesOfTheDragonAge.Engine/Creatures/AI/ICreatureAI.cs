﻿using CavesOfTheDragonAge.Common;

namespace CavesOfTheDragonAge.Engine.Creatures.AI
{
    /// <summary>
    /// An interface to control a creature's actions
    /// </summary>
    public interface ICreatureAI
    {
        /// <summary>
        /// Determine the next action for the creature to take.
        /// </summary>
        /// <returns>
        /// A CreatureAction containing the instructions for the
        /// next action for the creature to take.
        /// </returns>
        CreatureAction GetNextAction();

        /// <summary>
        /// The creature this AI is part of.
        /// </summary>
        Creature MyCreature { get; set; }

        //TODO: Add AI equality comparison.
    }
}