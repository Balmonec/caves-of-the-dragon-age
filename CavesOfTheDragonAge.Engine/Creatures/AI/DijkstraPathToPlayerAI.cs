﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CavesOfTheDragonAge.Common;

namespace CavesOfTheDragonAge.Engine.Creatures.AI
{
    class DijkstraPathToPlayerAI : ICreatureAI
    {
        #region [-- Public Methods --]

        /// <summary>
        /// Determine the next action for the creature to take.
        /// </summary>
        /// <returns>
        /// A CreatureAction containing the instructions for the
        /// next action for the creature to take.
        /// </returns>
        public CreatureAction GetNextAction()
        {
            Location myLocation = GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(MyCreature.Identifier);

            CreatureAction action;
            if (GameData.DungeonMaps[GameData.CurrentDungeon].mapToPlayer.GoalMap(myLocation.X, myLocation.Y) > 100)
            {
                MyCreature.Appearance.BackgroundColor = System.Drawing.Color.DarkSlateGray;
                action = new CreatureAction(CreatureActionType.Move, (Direction)Randomization.RandomInt(2, 9));
            }
            else
            {
                MyCreature.Appearance.BackgroundColor = System.Drawing.Color.Black;
                action = new CreatureAction(CreatureActionType.Move, GameData.DungeonMaps[GameData.CurrentDungeon].mapToPlayer.GetDirection(myLocation));
            }

            return action;
        }

        #endregion [-- Public Methods --]

        #region [-- Private Methods --]

        #endregion [-- Private Methods --]

        #region [-- Properties --]

        /// <summary>
        /// The creature this AI is part of.
        /// </summary>
        public Creature MyCreature { get; set; }

        #endregion [-- Properties --]
    }
}
