﻿namespace CavesOfTheDragonAge.Engine.Creatures.AI
{
    /// <summary>
    /// Shows the available types of player automation.
    /// </summary>
    public enum PlayerAutomationType
    {
        /// <summary>
        /// No automation, the user must control the player.
        /// </summary>
        None,

        /// <summary>
        /// Walking in a straight direction.
        /// </summary>
        Walking,

        /// <summary>
        /// Waiting in one place for a number of turns.
        /// </summary>
        Waiting
    }
}