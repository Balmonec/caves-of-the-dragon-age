﻿using System.Collections.Generic;
using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine.MagicSpells;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    /// <summary>
    /// A single square of the dungeon liquids.
    /// </summary>
    public class Fixture
    {
        #region [-- Public Methods --]

        /// <summary>
        /// Construct a fixture with default attributes.
        /// </summary>
        public Fixture()
        {
            Type = "NULL";
            Appearance = new DisplayCharacter();
            Attributes = new List<string>() { "NONE" };
        }

        /// <summary>
        /// Construct a fixture with attributes.
        /// </summary>
        /// <param name="type">The identifying name of the liquid.</param>
        /// <param name="appearance">The appearance of the liquid on the screen.</param>
        /// <param name="attributes">A list of any attributes on the liquid.</param>
        /// <param name="spell">A MagicSpell used by the fixture.</param>
        public Fixture(string type, DisplayCharacter appearance, bool blocksVision = false, bool blocksMovement = false, List<string> attributes = null, MagicSpell spell = null)
        {
            Type = type;
            Appearance = appearance;
            BlocksVision = blocksVision;
            BlocksMovement = blocksMovement;
            Attributes = attributes;
            Spell = spell;
        }

        /// <summary>
        /// Construct a new fixture from another fixture.
        /// </summary>
        /// <param name="fixture">A fixture to copy from.</param>
        public Fixture(Fixture fixture) : this(fixture.Type,
                                               new DisplayCharacter(fixture.Appearance),
                                               fixture.BlocksVision,
                                               fixture.BlocksMovement,
                                               new List<string>(fixture.Attributes),
                                               new MagicSpell(fixture.Spell))
        {
        }

        /// <summary>
        /// Returns a string containing the Type of the Fixture.
        /// </summary>
        /// <returns>The Type of the Fixture.</returns>
        public override string ToString()
        {
            return Type;
        }

        #endregion [-- Public Methods --]

        #region [-- Properties --]

        public DisplayCharacter Appearance { get; set; }

        public List<string> Attributes { get; set; }

        public string Type { get; set; }

        public MagicSpell Spell { get; set; }

        public bool BlocksVision = false;
        public bool BlocksMovement = false;

        #endregion [-- Properties --]
    }
}