﻿using System;
using System.Collections.Generic;
using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine.Creatures;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    public static partial class DungeonFactory
    {
        /// <summary>
        /// Generates a new Dungeon following the pattern of the map string, and returns it.
        /// </summary>
        /// <param name="map">A multi-line string containing the map to generate.</param>
        /// <returns>A new Dungeon, as defined by the simple map.</returns>
        private static Dungeon bySimpleMapGenerator(string map)
        {
            Dungeon mapDungeon = new Dungeon();

            mapDungeon.Name = "TESTING_GROUND";

            #region Split Maplines

            string[] mapLines = map.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            int height = mapLines.GetLength(0);
            int width = 0;
            for (int y = 0; y < height; y++)
            {
                if (mapLines[y].Length > width)
                {
                    width = mapLines[y].Length;
                }
            }

            #endregion Split Maplines

            #region Tiles

            Dictionary<string, string> tilesKey = new Dictionary<string, string>();
            tilesKey.Add("#", "STONE_WALL");
            tilesKey.Add("=", "STONE_CAVITY");
            tilesKey.Add("*", "STONE_CAVITY");
            tilesKey.Add(".", "STONE_FLOOR");
            tilesKey.Add("S", "STONE_FLOOR");
            tilesKey.Add("_", "STONE_FLOOR");
            tilesKey.Add("+", "STONE_FLOOR");
            tilesKey.Add("/", "STONE_FLOOR");
            tilesKey.Add("b", "STONE_CAVITY");
            tilesKey.Add("X", "STONE_FLOOR");
            tilesKey.Add(",", "STONE_FLOOR");
            tilesKey.Add("-", "STONE_WALL");
            tilesKey.Add("|", "STONE_FLOOR");
            tilesKey.Add("\'", "STONE_FLOOR");
            tilesKey.Add("U", "STONE_FLOOR");
            tilesKey.Add("<", "STONE_FLOOR");
            tilesKey.Add(">", "STONE_FLOOR");
            tilesKey.Add("@", "STONE_FLOOR");
            tilesKey.Add("o", "STONE_FLOOR");
            tilesKey.Add("1", "SOLID_WALL");
            tilesKey.Add("2", "SMOOTH_WALL");

            #region Make Tiles from Map

            string[,] tiles = new string[width, height];

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if (!tilesKey.TryGetValue(mapLines[y][x].ToString(), out tiles[x, y]))
                    {
                        tiles[x, y] = "NULL";
                    }
                }
            }

            #endregion Make Tiles from Map

            mapDungeon.Tiles = tiles;

            #endregion Tiles

            #region Liquids

            Dictionary<string, string> liquidsKey = new Dictionary<string, string>();
            liquidsKey.Add("*", "DEEP_WATER_WAVE_CREST");
            liquidsKey.Add("=", "DEEP_WATER");
            liquidsKey.Add("b", "DEEP_WATER");

            #region Make Liquids from Map

            string[,] liquids = new string[mapDungeon.Width, mapDungeon.Height];

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if (!liquidsKey.TryGetValue(mapLines[y][x].ToString(), out liquids[x, y]))
                    {
                        liquids[x, y] = "NULL";
                    }
                }
            }

            #endregion Make Liquids from Map

            mapDungeon.Liquids = liquids;

            #endregion Liquids

            #region Fixtures

            Dictionary<string, string> fixturesKey = new Dictionary<string, string>();
            fixturesKey.Add("S", "STATUE");
            fixturesKey.Add("_", "ALTAR");
            fixturesKey.Add("+", "CLOSED_DOOR");
            fixturesKey.Add("/", "OPEN_DOOR");
            fixturesKey.Add("b", "FIXED_BRIDGE");
            fixturesKey.Add("X", "HIDDEN_SECRET_DOOR");
            fixturesKey.Add(",", "LEVER_LOOSE");
            fixturesKey.Add("-", "ROPE_LOOSE");
            fixturesKey.Add("|", "CLOSED_GATE");
            fixturesKey.Add("U", "FOUNTAIN");

            #region Make Fixtures from Map

            string[,] fixtures = new string[width, height];

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if (!fixturesKey.TryGetValue(mapLines[y][x].ToString(), out fixtures[x, y]))
                    {
                        fixtures[x, y] = "NULL";
                    }
                }
            }

            #endregion Make Fixtures from Map

            mapDungeon.Fixtures = fixtures;

            #endregion Fixtures

            #region Creatures

            Dictionary<string, string> creaturesKey = new Dictionary<string, string>();
            creaturesKey.Add("@", "PLAYER");
            creaturesKey.Add("o", "ORC");

            mapDungeon.CreatureList = new Dictionary<string, Creature>();

            #region Make Creatures from Map

            string[,] creatures = new string[mapDungeon.Width, mapDungeon.Height];

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    string creatureType;

                    if (!creaturesKey.TryGetValue(mapLines[y][x].ToString(), out creatureType))
                    {
                        creatureType = "";
                    }

                    if (creatureType != "")
                    {
                        Creature newCreature = CreatureFactory.GetNewCreature(creatureType, mapDungeon.CreatureList.Count);
                        mapDungeon.CreatureList.Add(newCreature.Identifier, newCreature);
                        creatures[x, y] = newCreature.Identifier;
                    }
                    else
                    {
                        creatures[x, y] = "";
                    }
                }
            }

            #endregion Make Creatures from Map

            mapDungeon.Creatures = creatures;

            #endregion Creatures

            mapDungeon.ClearSFX();

            mapDungeon.mapToPlayer = new DijkstraMap(mapDungeon.Width, mapDungeon.Height);

            mapDungeon.UpdateDijkstraMaps();

            mapDungeon.ClearFOV();

            return mapDungeon;
        }
    }
}