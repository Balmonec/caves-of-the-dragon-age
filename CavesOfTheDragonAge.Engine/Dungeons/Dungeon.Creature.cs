﻿using System;
using System.Collections.Generic;
using System.Linq;
using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine.Creatures;
using System.Drawing;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    public partial class Dungeon
    {
        #region [-- Public Methods --]

        /// <summary>
        /// Get the Creature object for a given square.
        /// </summary>
        /// <param name="x">The X coordinate for the Creature to return.</param>
        /// <param name="y">The Y coordinate for the Creature to return.</param>
        /// <returns>The Creature object in the given square.</returns>
        public Creature GetCreature(int x, int y)
        {
            Creature currentCreature;

            if(!Creatures.IndexInBounds2D(x, y) || !CreatureList.TryGetValue(Creatures[x, y], out currentCreature))
            {
                currentCreature = null;
            }

            return currentCreature;
        }

        /// <summary>
        /// Get the Creature object for a given square.
        /// </summary>
        /// <param name="location">The Location for the Creature to return.</param>
        /// <returns>The Creature object in the given square.</returns>
        public Creature GetCreature(Location location)
        {
            return GetCreature(location.X, location.Y);
        }

        /// <summary>
        /// Get the Creature object for the given Id.
        /// </summary>
        /// <param name="creatureId">The Identifier of the Creature to return.</param>
        /// <returns>The Creature object with the given Id.</returns>
        public Creature GetCreature(string creatureId)
        {
            if (CreatureList.ContainsKey(creatureId))
            {
                return CreatureList[creatureId];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get the location of the specified creature.
        /// </summary>
        /// <param name="creatureId">The ID of the creature to locate.</param>
        /// <returns>A Location indicating the location of the specified creature.</returns>
        public Location GetCreatureLocation(string creatureId)
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    if (Creatures[x, y] == creatureId)
                    {
                        return new Location(x, y);
                    }
                }
            }

            return new Location(-1, -1);
        }

        /// <summary>
        /// Get the X coordinate of the specified creature.
        /// </summary>
        /// <param name="creatureId">The ID of the creature to locate.</param>
        /// <returns>The X coordinate of the specified creature.</returns>
        public int GetCreatureX(string creatureId)
        {
            return GetCreatureLocation(creatureId).Item1;
        }

        /// <summary>
        /// Get the Y coordinate of the specified creature.
        /// </summary>
        /// <param name="creatureId">The ID of the creature to locate.</param>
        /// <returns>The Y coordinate of the specified creature.</returns>
        public int GetCreatureY(string creatureId)
        {
            return GetCreatureLocation(creatureId).Item2;
        }

        /// <summary>
        /// Determine the list of passable directions from a given tile.
        /// </summary>
        /// <param name="location">The location of the tile to start from.</param>
        /// <param name="creature">The identifier of the creature to check for.
        /// <para>If none is given, will assume a creature of default properties.</para></param>
        /// <returns>A list of directions that are passable from the given tile.</returns>
        public List<Direction> GetPassableDirections(Location location, string creature = "")
        {
            return Enum.GetValues(typeof (Direction)).Cast<Direction>().
                Where(direction => IsDirectionPassable(location, direction, creature)).ToList();
        }

        /// <summary>
        /// Determine if a tile one square in a specific direction is passable.
        /// </summary>
        /// <param name="location">The location of the tile to start from.</param>
        /// <param name="direction">The direction to check.</param>
        /// <param name="creature">The identifier of the creature to check for.
        /// <para>If none is given, will assume a creature of default properties.</para></param>
        /// <returns>Returns true if the tile is passable by the given creature, and false if it is not.</returns>
        public bool IsDirectionPassable(Location location, Direction direction, string creature = "")
        {
            if (direction.IsHorizontalMovement())
            {
                return IsPassable(location.GetVector(direction), creature);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Determine if a specific tile is passable.
        /// </summary>
        /// <param name="x">The X coordinate of the tile to check.</param>
        /// <param name="y">The Y coordinate of the tile to check.</param>
        /// <param name="creature">The identifier of the creature to check for.
        /// <para>If none is given, will assume a creature of default properties.</para></param>
        /// <returns>Returns true if the tile is passable by the given creature, and false if it is not.</returns>
        public bool IsPassable(int x, int y, string creature = "")
        {
            if ((x < 0) || (y < 0) || (x >= Width) || (y >= Height))
            {
                return false;
            }
            else if (GetFixture(x, y).BlocksMovement)
            {
                return false;
            }
            else if (GetLiquid(x, y).BlocksMovement)
            {
                return false;
            }
            else if (GetTile(x, y).BlocksMovement)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Determine if a specific tile is passable.
        /// </summary>
        /// <param name="location">The location of the tile to check.</param>
        /// <param name="creature">The identifier of the creature to check for.
        /// <para>If none is given, will assume a creature of default properties.</para></param>
        /// <returns>Returns true if the tile is passable by the given creature, and false if it is not.</returns>
        public bool IsPassable(Location location, string creature = "")
        {
            return IsPassable(location.X, location.Y, creature);
        }

        /// <summary>
        /// Move the creature in a specified direction.
        /// </summary>
        /// <param name="creatureId">The ID of the creature to move.</param>
        /// <param name="direction">The direction to move the creature.</param>
        /// <returns>Returns true if the movement is successful, and false otherwise.</returns>
        public bool MoveCreature(string creatureId, Direction direction)
        {
            int oldX = GetCreatureX(creatureId);
            int oldY = GetCreatureY(creatureId);

            int newX = oldX + direction.GetDeltaX();
            int newY = oldY + direction.GetDeltaY();

            if (IsPassable(newX, newY) == false)
            {
                //TODO: Add a message that the creature has bumped into the wall.
                return false;
            }
            else if (Creatures[newX, newY] != "")
            {
                if (FOVState[newX, newY] == FOVStateType.Visible)
                {
                    string thisCreatureName = CreatureList[creatureId].Name;
                    string thatCreatureName = CreatureList[Creatures[newX, newY]].Name;
                    GameData.Ui.DisplayMessage(thisCreatureName + " bumps into " + thatCreatureName + "!", Color.Red);
                }
                return false;
            }

            Creatures[newX, newY] = creatureId;
            Creatures[oldX, oldY] = "";

            return true;
        }

        /// <summary>
        /// Make the creature attack in a specified direction.
        /// </summary>
        /// <param name="attackerId">The ID of the creature performing the attack.</param>
        /// <param name="direction">The direction to attack in.</param>
        /// <returns></returns>
        public void Attack(string attackerId, Direction direction)
        {
            Location attackerLocation = GetCreatureLocation(attackerId);
            Location targetLocation = attackerLocation.GetVector(direction);
            Creature attackerCreature = CreatureList[attackerId];
            Creature targetCreature = GetCreature(targetLocation);

            if(targetCreature == null)
            {
                if (FOVState[attackerLocation.X, attackerLocation.Y] == FOVStateType.Visible)
                {
                    GameData.Ui.DisplayMessage(attackerCreature.Name + " swings wildly at the air to the " + direction.GetDescription() + "!", Color.White);
                }
            }
            else
            {
                int damage = Randomization.RandomInt(1, 6);
                if (FOVState[attackerLocation.X, attackerLocation.Y] == FOVStateType.Visible)
                {
                    if (FOVState[targetLocation.X, targetLocation.Y] == FOVStateType.Visible)
                    {
                        GameData.Ui.DisplayMessage(attackerCreature.Name + " hits " + targetCreature.Name + " for " + damage + " hit points of damage!", Color.Red);
                    }
                    else
                    {
                        GameData.Ui.DisplayMessage(attackerCreature.Name + " hits something!", Color.Red);
                    }
                }
                else
                {
                    if (FOVState[targetLocation.X, targetLocation.Y] == FOVStateType.Visible)
                    {
                        GameData.Ui.DisplayMessage("Something hits " + targetCreature.Name + " for " + damage + " hit points of damage!", Color.Red);
                    }
                }
                targetCreature.Health = targetCreature.Health - damage;
                attackerCreature.Score -= damage;
                targetCreature.Score -= damage;
            }
        }

        /// <summary>
        /// Kill the specified creature.
        /// </summary>
        /// <param name="creatureId">The ID of the creature to kill.</param>
        public void KillCreature(string creatureId)
        {
            Creature creature = GetCreature(creatureId);
            Location creatureLocation = GetCreatureLocation(creatureId);
            Creatures[creatureLocation.X, creatureLocation.Y] = "";
            if(FOVState[creatureLocation.X, creatureLocation.Y] == FOVStateType.Visible && creatureId != "PLAYER")
            {
                GameData.Ui.DisplayMessage(creature.Name + " has died!", GameColors.Red);
            }
            else if (creatureId == "PLAYER")
            {
                GameData.Ui.RenderAll();
                int choice = GameData.Ui.DisplayMenu("You have failed your entrance exam." + Environment.NewLine
                                                   + "Would you like to try again?", new List<string>() { "Yes", "No" }, true);
                if(choice == 0)
                {
                    GameData.StartOver = true;
                }
                else
                {
                    GameData.EndGame = true;
                }
            }
        }

        /// <summary>
        /// Teleport the specified creature to the specified location.
        /// </summary>
        /// <param name="creatureId">The ID of the creature to teleport.</param>
        /// <param name="destination">The location to teleport to.</param>
        public void TeleportCreature(string creatureId, Location destination)
        {
            if (IsPassable(destination) != false && Creatures[destination.X, destination.Y] == "")
            {
                if (GetCreatureX(creatureId) != -1)
                {
                    Creatures[GetCreatureX(creatureId), GetCreatureY(creatureId)] = "";
                }
                Creatures[destination.X, destination.Y] = creatureId;
            }
        }

        ///// <summary>
        ///// Determines the PathwayType of the square.
        ///// </summary>
        ///// <param name="location">The location of the square to check.</param>
        ///// <param name="creature">The identifier of the creature to check for.
        ///// <para>If none is given, will assume a creature of default properties.</para></param>
        ///// <returns>The PathwayType of the square, as determined by its surroundings.</returns>
        //public PathwayType GetPathwayType(Location location, string creature = "")
        //{
        //    List<Direction> passableDirections = GetPassableDirections(location, creature);

        //    if (IsPassable(location, creature) == false)
        //    {
        //        return PathwayType.Impassable;
        //    }
        //    else if (passableDirections.Count == 1)
        //    {
        //        return PathwayType.DeadEnd;
        //    }
        //    else if (passableDirections.Count >= 3)
        //    {
        //        return PathwayType.Room;
        //    }
        //    else if (passableDirections.Count == 2)
        //    {
        //        if (passableDirections.Contains(passableDirections[0].Opposite()))
        //        {
        //            return PathwayType.CorridorStraight;
        //        }
        //        else
        //        {
        //            return PathwayType.CorridorCorner;
        //        }
        //    }
        //    else
        //    {
        //        return PathwayType.Undefined;
        //    }
        //}

        #endregion [-- Public Methods --]

        #region [-- Properties --]

        /// <summary>
        /// A Dictionary holding the Creature objects.
        /// </summary>
        public Dictionary<string, Creature> CreatureList { get; set; }

        /// <summary>
        /// An array of strings indicating the map of Creatures.
        /// </summary>
        public string[,] Creatures { get; set; }

        #endregion [-- Properties --]
    }
}