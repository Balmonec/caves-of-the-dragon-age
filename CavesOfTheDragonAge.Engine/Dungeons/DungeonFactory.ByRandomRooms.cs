﻿using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine.Creatures;
using CavesOfTheDragonAge.Engine.MagicSpells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    public static partial class DungeonFactory
    {
        private static Dungeon byRandomRooms(string name, Tuple<int, int> size, Creature player = null)
        {
            const int MAX_ROOMS = 30;
            const int ROOM_MIN_SIZE = 2;
            const int ROOM_MAX_SIZE = 10;

            Dungeon dungeon = new Dungeon();

            dungeon.Name = name;

            #region fill all arrays

            dungeon.Tiles = new string[size.Item1, size.Item2];
            dungeon.fillTiles();

            dungeon.Liquids = new string[size.Item1, size.Item2];
            dungeon.fillLiquids();

            dungeon.Fixtures = new string[size.Item1, size.Item2];
            dungeon.fillFixtures();

            dungeon.Creatures = new string[size.Item1, size.Item2];
            dungeon.fillCreatures();
            dungeon.CreatureList = new Dictionary<string, Creature>();

            dungeon.ClearSFX();

            #endregion fill all arrays

            dungeon.fillTiles("STONE_WALL");

            List<Rect> rooms = new List<Rect>();

            for(int r = 0; r<MAX_ROOMS; r++)
            {
                //Get a random width and height
                int width = Randomization.RandomInt(ROOM_MIN_SIZE, ROOM_MAX_SIZE, true);
                int height = Randomization.RandomInt(ROOM_MIN_SIZE, ROOM_MAX_SIZE, true);

                //Get a random position inside the bounds of the map
                int x = Randomization.RandomInt(0, size.Item1 - width, false);
                int y = Randomization.RandomInt(0, size.Item2 - height, false);

                Rect newRoom = new Rect(x, y, width, height);

                //Check if there are any intersections with the other rooms
                bool intersects = false;
                foreach (Rect room in rooms)
                {
                    if (newRoom.Intersects(room) == true)
                    {
                        intersects = true;
                        break;
                    }
                }

                if (intersects == false)
                {
                    //Place the room on the map
                    dungeon.fillTiles("STONE_FLOOR", newRoom);

                    if(rooms.Count == 0)
                    {
                        //This is the first room, the player starts here
                        if (player == null)
                        {
                            player = CreatureFactory.GetNewCreature("PLAYER", dungeon.CreatureList.Count);
                        }
                        dungeon.CreatureList.Add(player.Identifier, player);
                        dungeon.Creatures[newRoom.CenterX(), newRoom.CenterY()] = player.Identifier;

                        dungeon.tryPlaceSpellRune(newRoom.RandomX(), newRoom.RandomY(), MagicSpellFactory.GetRandomStockSpell());
                    }
                    else
                    {
                        //all rooms after the first:
                        //connect it to a previous room with a tunnel
                        Rect oldRoom = rooms[Randomization.RandomInt(0, rooms.Count - 1, true)];
                        dungeon.digTunnel(newRoom.Center(), oldRoom.Center());

                        //Possibly fill it with a lake
                        if (Randomization.RandomInt(1, 4, true) == 1)
                        {
                            Rect lake = new Rect(x + 1, y + 1, width - 2, height - 2);
                            dungeon.fillTiles("STONE_CAVITY", lake);
                            dungeon.fillLiquids("DEEP_WATER", lake);
                        }
                        else
                        {
                            //otherwise, add monsters
                            string creatureType = "";

                            switch (Randomization.RandomInt(1, 11, true))
                            {
                                case 1:
                                case 2:
                                    creatureType = "ORC";
                                    break;

                                case 3:
                                case 4:
                                    creatureType = "GOBLIN";
                                    break;

                                case 5:
                                case 6:
                                    creatureType = "KOBOLD";
                                    break;

                                case 7:
                                    creatureType = "FIREELEMENTAL";
                                    break;

                                case 8:
                                    creatureType = "WATERELEMENTAL";
                                    break;

                                case 9:
                                    creatureType = "AIRELEMENTAL";
                                    break;

                                case 10:
                                    creatureType = "EARTHELEMENTAL";
                                    break;

                                case 11:
                                    creatureType = "ARCANEANOMALLY";
                                    break;
                            }

                            Creature newCreature = CreatureFactory.GetNewCreature(creatureType, dungeon.CreatureList.Count);
                            dungeon.CreatureList.Add(newCreature.Identifier, newCreature);
                            dungeon.Creatures[newRoom.CenterX(), newRoom.CenterY()] = newCreature.Identifier;

                            //consider adding a spellrune
                            if(Randomization.RandomInt(1, 3)==1)
                            {
                                dungeon.tryPlaceSpellRune(newRoom.RandomX(), newRoom.RandomY(), MagicSpellFactory.GetRandomStockSpell());
                            }
                        }
                    }

                    //finally, add the new room to the list
                    rooms.Add(newRoom);
                }
            }

            dungeon.mapToPlayer = new DijkstraMap(dungeon.Width, dungeon.Height);
            dungeon.UpdateDijkstraMaps();

            Rect stairRoom = rooms.OrderBy(r => dungeon.mapToPlayer.GoalMap(r.CenterX(), r.CenterY()) == int.MaxValue ? 0 : dungeon.mapToPlayer.GoalMap(r.CenterX(), r.CenterY())).Last();

            dungeon.Tiles[stairRoom.CenterX(), stairRoom.CenterY()] = "STONE_DOWNWARD_STAIR";

            for (int x = 0; x < size.Item1; x++)
            {
                for (int y = 0; y < size.Item2; y++)
                {
                    if (dungeon.Fixtures[x, y] == "NULL" && dungeon.IsPossibleDoorway(x, y))
                    {
                        switch (Randomization.RandomInt(1, 25, true))
                        {
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                                dungeon.Fixtures[x, y] = "CLOSED_DOOR";
                                break;

                            case 6:
                                dungeon.Fixtures[x, y] = "HIDDEN_SECRET_DOOR";
                                break;

                            default:
                                break;
                        }
                    }
                }
            }

            dungeon.lineEdges("WALL_OF_FORCE");

            dungeon.UpdateDijkstraMaps();

            dungeon.ClearFOV();

            return dungeon;
        }
    }
}
