﻿using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine.Creatures;
using CavesOfTheDragonAge.Engine.MagicSpells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    public partial class Dungeon
    {
        public void CastMagicSpell(MagicSpell spell, Location targetLocation, Location casterLocation, Creature caster)
        {

            foreach (SpellEffect spellEffect in spell.EffectList)
            {
                List<Location> effectLocations = spellEffect.AreaType.GetAreaLocations(targetLocation, spellEffect.AreaMagnitude, casterLocation);

                effectLocations.RemoveAll(loc => IsInDungeon(loc) == false);

                switch (spellEffect.EffectType)
                {
                    case EffectType.Heal:
                        {
                            foreach (Location effectLocation in effectLocations)
                            {
                                if (spellEffect.EffectAppearance != null)
                                {
                                    SFXLayer[effectLocation.X, effectLocation.Y] = spellEffect.EffectAppearance;
                                }

                                Creature creature = GetCreature(effectLocation);

                                if (creature != null)
                                {
                                    var healBy = Math.Min(creature.Health.Maximum - creature.Health.Current, spellEffect.EffectMagnitude);
                                    creature.Health += spellEffect.EffectMagnitude;
                                    if(creature.Identifier == caster.Identifier)
                                    {
                                        caster.Score += healBy;
                                    }
                                    else
                                    {
                                        caster.Score -= healBy;
                                    }

                                    if (spellEffect.EffectAppearance != null && FOVState[effectLocation.X, effectLocation.Y] == FOVStateType.Visible)
                                    {
                                        GameData.Ui.DisplayMessage(string.Format(spellEffect.EffectDescription, creature.Name, spellEffect.EffectMagnitude), spellEffect.EffectAppearance.ForegroundColor);
                                    }
                                }
                            }

                            if (spellEffect.EffectAppearance != null)
                            {
                                GameData.Ui.RenderAll(true);
                            }
                        }
                        break;

                    case EffectType.Damage:
                        {
                            foreach (Location effectLocation in effectLocations)
                            {
                                if (spellEffect.EffectAppearance != null)
                                {
                                    SFXLayer[effectLocation.X, effectLocation.Y] = spellEffect.EffectAppearance;
                                }

                                Creature creature = GetCreature(effectLocation);

                                if (creature != null)
                                {
                                    var damageBy = Math.Min(creature.Health.Current, spellEffect.EffectMagnitude);
                                    creature.Health -= spellEffect.EffectMagnitude;
                                    if (creature.Identifier == caster.Identifier)
                                    {
                                        caster.Score -= damageBy;
                                    }
                                    else
                                    {
                                        caster.Score += damageBy;
                                    }

                                    if (spellEffect.EffectAppearance != null && FOVState[effectLocation.X, effectLocation.Y] == FOVStateType.Visible)
                                    {
                                        GameData.Ui.DisplayMessage(string.Format(spellEffect.EffectDescription, creature.Name, spellEffect.EffectMagnitude), spellEffect.EffectAppearance.ForegroundColor);
                                    }
                                }
                            }

                            if (spellEffect.EffectAppearance != null)
                            {
                                GameData.Ui.RenderAll(true);
                            }
                        }
                        break;

                    case EffectType.Trigger:
                        {
                            if (spellEffect.EffectAppearance != null)
                            {
                                GameData.Ui.DisplayMessage(string.Format(spellEffect.EffectDescription), spellEffect.EffectAppearance.ForegroundColor);
                            }
                            foreach (Location effectLocation in effectLocations)
                            {
                                if (spellEffect.EffectAppearance != null)
                                {
                                    SFXLayer[effectLocation.X, effectLocation.Y] = spellEffect.EffectAppearance;
                                }

                                TriggerCell(effectLocation, spellEffect.EffectTag);
                            }

                            if (spellEffect.EffectAppearance != null)
                            {
                                GameData.Ui.RenderAll(true);
                            }
                            ImpassableCells = null;
                            TransparentCells = null;
                            UpdateDijkstraMaps();
                        }
                        break;

                    case EffectType.Teleport:
                        {
                            effectLocations.RemoveAll(loc => IsPassable(loc) == false);
                            effectLocations.RemoveAll(loc => GetCreature(loc) != null);

                            Creature creature = GetCreature(casterLocation);

                            if (effectLocations.Count() == 0 || creature == null)
                            {
                                GameData.Ui.DisplayMessage(string.Format(spellEffect.EffectTag), spellEffect.EffectAppearance.ForegroundColor);
                            }
                            else
                            {
                                Location newLocation = effectLocations[Randomization.RandomInt(0, effectLocations.Count() - 1, true)];
                                TeleportCreature(creature.Identifier, newLocation);

                                if (spellEffect.EffectAppearance != null)
                                {
                                    SFXLayer[newLocation.X, newLocation.Y] = spellEffect.EffectAppearance;
                                    SFXLayer[casterLocation.X, casterLocation.Y] = spellEffect.EffectAppearance;
                                    GameData.Ui.DisplayMessage(string.Format(spellEffect.EffectDescription, creature.Name, spellEffect.EffectMagnitude), spellEffect.EffectAppearance.ForegroundColor);
                                    GameData.Ui.RenderAll(true);
                                }
                            }
                        }
                        break;

                    case EffectType.None:
                    default:
                        break;
                }
            }
        }

        public void ClearSFX()
        {
            SFXLayer = new DisplayCharacter[Width, Height];
        }

        public DisplayCharacter[,] SFXLayer;
    }
}
