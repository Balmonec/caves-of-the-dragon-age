﻿using System.Collections.Generic;
using CavesOfTheDragonAge.Common;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    /// <summary>
    /// A single square of the dungeon liquids.
    /// </summary>
    public class Liquid
    {
        #region [-- Public Methods --]

        /// <summary>
        /// Construct a liquid with default attributes.
        /// </summary>
        public Liquid()
        {
            Type = "NULL";
            Appearance = new DisplayCharacter();
            Attributes = new List<string>() { "NONE" };
        }

        /// <summary>
        /// Construct a liquid with attributes.
        /// </summary>
        /// <param name="type">The identifying name of the liquid.</param>
        /// <param name="appearance">The appearance of the liquid on the screen.</param>
        /// <param name="attributes">A list of any attributes on the liquid.</param>
        public Liquid(string type, DisplayCharacter appearance, bool blocksVision = false, bool blocksMovement = false, List<string> attributes = null)
        {
            Type = type;
            Appearance = appearance;
            BlocksVision = blocksVision;
            BlocksMovement = blocksMovement;
            Attributes = attributes;
        }

        /// <summary>
        /// Returns a string containing the Type of the Liquid.
        /// </summary>
        /// <returns>The Type of the Liquid.</returns>
        public override string ToString()
        {
            return Type;
        }

        #endregion [-- Public Methods --]

        #region [-- Properties --]

        public DisplayCharacter Appearance { get; set; }

        public List<string> Attributes { get; set; }

        public string Type { get; set; }

        public bool BlocksVision = false;
        public bool BlocksMovement = false;

        #endregion [-- Properties --]
    }
}