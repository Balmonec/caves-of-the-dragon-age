﻿using CavesOfTheDragonAge.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    public partial class Dungeon
    {
        public void UpdateDijkstraMaps()
        {
            //TODO: Generalize this into a generic faction map system.

            bool[,] goalCells = new bool[Width, Height];

            for (int x = 0; x < Width; x++)
            {
                for(int y = 0; y<Height; y++)
                {
                    if(Creatures[x, y]=="PLAYER")
                    {
                        goalCells[x, y] = true;
                    }
                }
            }

            RebuildCellStates();

            mapToPlayer.BuildMap(goalCells, ImpassableCells);
        }

        public bool[,] ImpassableCells;

        //TODO: Generalize this into a generic faction map system.
        public DijkstraMap mapToPlayer;
    }
}
