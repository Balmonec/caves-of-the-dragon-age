﻿using System;
using System.Collections.Generic;
using System.Drawing;
using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine.Dungeons;

namespace CavesOfTheDragonAge.Engine
{
    /// <summary>
    /// A static class containing the game data.
    /// </summary>
    public static class GameData
    {
        #region [-- Public Methods --]

        /// <summary>
        /// Loads the Tiles, Liquids, and Features, then sets the UI.
        /// </summary>
        /// <param name="userInterface">The IUserInterface to use as the UI.</param>
        public static void Setup(IUserInterface userInterface)
        {
            AutomationWaitTime = 50;
            loadTiles();
            loadLiquids();
            loadFixtures();
            Ui = userInterface;
        }

        #endregion [-- Public Methods --]

        #region [-- Private Methods --]

        /// <summary>
        /// Load the list of available Fixtures.
        /// </summary>
        private static void loadFixtures()
        {
            Fixtures = new Dictionary<string, Fixture>();
            
            Fixtures.Add("NULL",
                new Fixture("NULL",
                    null,
                    false,
                    false,
                    new List<string>() { "ON_TRIGGER:FROZEN:ALTER_SELF:ICE" }));

            Fixtures.Add("STATUE",
                new Fixture("STATUE",
                    new DisplayCharacter(Convert.ToChar(8486), Color.DarkGray),
                    false,
                    false,
                    new List<string>()));

            Fixtures.Add("ALTAR",
                new Fixture("ALTAR",
                    new DisplayCharacter('_', Color.White),
                    false,
                    false,
                    new List<string>()));

            Fixtures.Add("CLOSED_DOOR",
                new Fixture("CLOSED_DOOR",
                    new DisplayCharacter('+', GameColors.Brown),
                    true,
                    true,
                    new List<string>() { "ON_TRIGGER:OPEN:ALTER_SELF:OPEN_DOOR" }));

            Fixtures.Add("OPEN_DOOR",
                new Fixture("OPEN_DOOR",
                    new DisplayCharacter('/', GameColors.Brown),
                    false,
                    false,
                    new List<string>() { "ON_TRIGGER:CLOSE:ALTER_SELF:CLOSED_DOOR" }));

            Fixtures.Add("FIXED_BRIDGE",
                new Fixture("FIXED_BRIDGE",
                    new DisplayCharacter('=', GameColors.Brown),
                    false,
                    false,
                    new List<string>()));

            Fixtures.Add("HIDDEN_SECRET_DOOR",
                new Fixture("HIDDEN_SECRET_DOOR",
                    new DisplayCharacter('#', Color.DarkGray),
                    true,
                    true,
                    new List<string>() { "ON_TRIGGER:OPEN:ALTER_SELF:OPEN_SECRET_DOOR" }));

            Fixtures.Add("CLOSED_SECRET_DOOR",
                new Fixture("CLOSED_SECRET_DOOR",
                    new DisplayCharacter('+', GameColors.Brown),
                    true,
                    true,
                    new List<string>() { "ON_TRIGGER:HANDLE:ALTER_SELF:HIDDEN_SECRET_DOOR",
                                         "ON_TRIGGER:OPEN:ALTER_SELF:OPEN_SECRET_DOOR" }));

            Fixtures.Add("OPEN_SECRET_DOOR",
                new Fixture("OPEN_SECRET_DOOR",
                    new DisplayCharacter('/', GameColors.Brown),
                    false,
                    false,
                    new List<string>() { "ON_TRIGGER:HANDLE:ALTER_SELF:HIDDEN_SECRET_DOOR",
                                         "ON_TRIGGER:CLOSE:ALTER_SELF:CLOSED_SECRET_DOOR" }));

            Fixtures.Add("LEVER_LOOSE",
                new Fixture("LEVER_LOOSE",
                    new DisplayCharacter(Convert.ToChar(242), Color.Gray),
                    false,
                    false,
                    new List<string>() { "ON_TRIGGER:HANDLE:ALTER_SELF_IMMEDIATE:LEVER_TAUGHT",
                                         "ON_TRIGGER:HANDLE:TRIGGER_ADJACENT:TIGHTEN_ROPE",
                                         "ON_TRIGGER:TIGHTEN_ROPE:ALTER_SELF_IMMEDIATE:LEVER_TAUGHT",
                                         "ON_TRIGGER:TIGHTEN_ROPE:TRIGGER_ADJACENT:TIGHTEN_ROPE" }));

            Fixtures.Add("LEVER_TAUGHT",
                new Fixture("LEVER_TAUGHT",
                    new DisplayCharacter(Convert.ToChar(243), Color.Gray),
                    false,
                    false,
                    new List<string>() { "ON_TRIGGER:HANDLE:ALTER_SELF_IMMEDIATE:LEVER_LOOSE",
                                         "ON_TRIGGER:HANDLE:TRIGGER_ADJACENT:LOOSEN_ROPE",
                                         "ON_TRIGGER:LOOSEN_ROPE:ALTER_SELF_IMMEDIATE:LEVER_LOOSE",
                                         "ON_TRIGGER:LOOSEN_ROPE:TRIGGER_ADJACENT:LOOSEN_ROPE" }));

            Fixtures.Add("ROPE_LOOSE",
                new Fixture("ROPE_LOOSE",
                    new DisplayCharacter('~', GameColors.Brown),
                    false,
                    false,
                    new List<string>() { "ON_TRIGGER:TIGHTEN_ROPE:ALTER_SELF_IMMEDIATE:ROPE_TAUGHT",
                                         "ON_TRIGGER:TIGHTEN_ROPE:TRIGGER_ADJACENT:TIGHTEN_ROPE" }));

            Fixtures.Add("ROPE_TAUGHT",
                new Fixture("ROPE_TAUGHT",
                    new DisplayCharacter('-', GameColors.Brown),
                    false,
                    false,
                    new List<string>() { "ON_TRIGGER:LOOSEN_ROPE:ALTER_SELF_IMMEDIATE:ROPE_LOOSE",
                                         "ON_TRIGGER:LOOSEN_ROPE:TRIGGER_ADJACENT:LOOSEN_ROPE" }));

            Fixtures.Add("CLOSED_GATE",
                new Fixture("CLOSED_GATE",
                    new DisplayCharacter('#', GameColors.Brown),
                    false,
                    false,
                    new List<string>() { "BLOCKS_MOVEMENT",
                                         "ON_TRIGGER:TIGHTEN_ROPE:ALTER_SELF_IMMEDIATE:OPEN_GATE",
                                         "ON_TRIGGER:TIGHTEN_ROPE:TRIGGER_ADJACENT:TIGHTEN_ROPE" }));

            Fixtures.Add("OPEN_GATE",
                new Fixture("OPEN_GATE",
                    new DisplayCharacter('|', GameColors.SaddleBrown),
                    false,
                    false,
                    new List<string>() { "ON_TRIGGER:LOOSEN_ROPE:ALTER_SELF_IMMEDIATE:CLOSED_GATE",
                                         "ON_TRIGGER:LOOSEN_ROPE:TRIGGER_ADJACENT:LOOSEN_ROPE" }));

            Fixtures.Add("FOUNTAIN",
                new Fixture("FOUNTAIN",
                    new DisplayCharacter(Convert.ToChar(383), GameColors.Blue),
                    false,
                    false,
                    new List<string>()));

            Fixtures.Add("ICE",
                new Fixture("ICE",
                    new DisplayCharacter('#', Color.White),
                    false,
                    false,
                    new List<string>() { "ON_TRIGGER:BURN:ALTER_SELF:NULL",
                                         "ON_TRIGGER:WAVE:ALTER_SELF:NULL" }));

            Fixtures.Add("SPELLRUNE",
                new Fixture("SPELLRUNE",
                    new DisplayCharacter('\u03D5', GameColors.BlueViolet),
                    false,
                    false,
                    new List<string>() { "ON_CREATURE:TEACH_SPELL" }));
        }

        /// <summary>
        /// Load the list of available Liquids.
        /// </summary>
        private static void loadLiquids()
        {
            Liquids = new Dictionary<string, Liquid>();

            Liquids.Add("DEEP_WATER",
                new Liquid("DEEP_WATER",
                    new DisplayCharacter('=', GameColors.Blue),
                    false,
                    true,
                    new List<string>() { "ON_TRIGGER:WAVE:ALTER_SELF:DEEP_WATER_WAVE_CREST",
                                         "ON_TRIGGER:FREEZE:TRIGGER_SELF:FROZEN",
                                         "ON_TRIGGER:BURN:ALTER_SELF:DEEP_WATER_WAVE_CREST" }));

            Liquids.Add("DEEP_WATER_WAVE_CREST",
                new Liquid("DEEP_WATER_WAVE_CREST",
                    new DisplayCharacter('*', GameColors.LightBlue),
                    false,
                    true,
                    new List<string>() { "TRIGGER_ADJACENT:WAVE",
                                         //"ALTER_ADJACENT_LIQUID:DEEP_WATER:DEEP_WATER_WAVE_CREST",
                                         "ALTER_SELF:DEEP_WATER_WAVE_BODY" }));

            Liquids.Add("DEEP_WATER_WAVE_BODY",
                new Liquid("DEEP_WATER_WAVE_BODY",
                    new DisplayCharacter('#', GameColors.LightBlue),
                    false,
                    true,
                    new List<string>() { "ALTER_SELF:DEEP_WATER_ROUGH" }));

            Liquids.Add("DEEP_WATER_ROUGH",
                new Liquid("DEEP_WATER_ROUGH",
                    new DisplayCharacter('#', GameColors.Blue),
                    false,
                    true,
                    new List<string>() { "ALTER_SELF:DEEP_WATER",
                                         "ON_TRIGGER:FREEZE:TRIGGER_SELF:FROZEN" }));
        }

        /// <summary>
        /// Load the list of available Tiles.
        /// </summary>
        private static void loadTiles()
        {
            Tiles = new Dictionary<string, Tile>();

            Tiles.Add("STONE_WALL",
                new Tile("STONE_WALL",
                    new DisplayCharacter('#', Color.DarkGray),
                    true,
                    true,
                    new List<string>() { "CLASS:POSSIBLE_DOORFRAME",
                                         "ON_TRIGGER:MOSS:ALTER_SELF:STONE_WALL_MOSSY",
                                         "ON_TRIGGER:DIG:ALTER_SELF:STONE_FLOOR"}));

            Tiles.Add("STONE_FLOOR",
                new Tile("STONE_FLOOR",
                    new DisplayCharacter('.', Color.LightGray),
                    false,
                    false,
                    new List<string>() { "CLASS:POSSIBLE_DOORSILL",
                                         "CLASS:POSSIBLE_DOORWAY",
                                         "ON_TRIGGER:MOSS:ALTER_SELF:STONE_FLOOR_MOSSY" }));

            Tiles.Add("STONE_DOWNWARD_STAIR",
                new Tile("STONE_DOWNWARD_STAIR",
                    new DisplayCharacter('>', Color.White, Color.DarkGray),
                    false,
                    false,
                    new List<string>() { "ON_CREATURE:MOVE_DOWNWARD" }));

            Tiles.Add("STONE_CAVITY",
                new Tile("STONE_CAVITY",
                    new DisplayCharacter('.', Color.Gray),
                    false,
                    false,
                    new List<string>() { "HOLDS_LIQUID" }));

            Tiles.Add("STONE_WALL_MOSSY",
                new Tile("STONE_WALL_MOSSY",
                    new DisplayCharacter('#', Color.DarkGreen),
                    true,
                    true,
                    new List<string>() { "CLASS:POSSIBLE_DOORFRAME",
                                         "TRIGGER_ADJACENT:MOSS" }));

            Tiles.Add("STONE_FLOOR_MOSSY",
                new Tile("STONE_FLOOR_MOSSY",
                    new DisplayCharacter('.', Color.DarkGreen),
                    false,
                    false,
                    new List<string>() { "CLASS:POSSIBLE_DOORSILL",
                                         "CLASS:POSSIBLE_DOORWAY",
                                         "TRIGGER_ADJACENT:MOSS" }));

            Tiles.Add("SOLID_WALL",
                new Tile("SOLID_WALL",
                    new DisplayCharacter('\u2588', Color.DarkGray),
                    true,
                    true,
                    new List<string>() { "CLASS:POSSIBLE_DOORFRAME" }));

            Tiles.Add("SMOOTH_WALL",
                new Tile("SMOOTH_WALL",
                    new DisplayCharacter('\u256C', Color.DarkGray),
                    true,
                    true,
                    new List<string>() { "CLASS:POSSIBLE_DOORFRAME" }));

            Tiles.Add("WALL_OF_FORCE",
                new Tile("WALL_OF_FORCE",
                    new DisplayCharacter('#', Color.GreenYellow),
                    true,
                    true,
                    new List<string>() { "CLASS:POSSIBLE_DOORFRAME"}));
        }

        #endregion [-- Private Methods --]

        #region [-- Properties --]

        /// <summary>
        /// The number of milliseconds to wait in between actions when automatically moving the player.
        /// </summary>
        public static int AutomationWaitTime { get; set; }

        /// <summary>
        /// The identifier of the current dungeon being used.
        /// </summary>
        public static string CurrentDungeon { get; set; }

        /// <summary>
        /// The list of Dungeons in the game.
        /// </summary>
        public static Dictionary<string, Dungeon> DungeonMaps { get; set; }

        //TODO figure out a more elegant way to end the game
        //This means the public static bool, not the isWindowClosed hack.
        public static bool EndGame { get; set; }

        /// <summary>
        /// The list of available Fixtures.
        /// </summary>
        public static Dictionary<string, Fixture> Fixtures { get; set; }

        /// <summary>
        /// The list of available Liquids.
        /// </summary>
        public static Dictionary<string, Liquid> Liquids { get; set; }

        /// <summary>
        /// The list of available Tiles.
        /// </summary>
        public static Dictionary<string, Tile> Tiles { get; set; }

        /// <summary>
        /// The user interface currently configured to be used.
        /// </summary>
        public static IUserInterface Ui { get; set; }

        public const int MaximumDepth = 3;

        public static int Depth { get; set; }

        public static bool GoDown = false;

        public static bool StartOver = false;

        #endregion [-- Properties --]
    }
}