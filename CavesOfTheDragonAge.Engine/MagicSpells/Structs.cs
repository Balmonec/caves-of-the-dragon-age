﻿using CavesOfTheDragonAge.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Engine.MagicSpells
{
    public class SpellTarget
    {
        public SpellTarget()
        {
            TargetType = TargetType.None;
            Magnitude = 0;
        }

        public SpellTarget(SpellTarget spellTarget)
        {
            TargetType = spellTarget.TargetType;
            Magnitude = spellTarget.Magnitude;
        }

        public TargetType TargetType;

        public int Magnitude;

        public static bool operator ==(SpellTarget left, SpellTarget right)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(left, right))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)left == null) || ((object)right == null))
            {
                return false;
            }

            // Return true if the fields match:
            return left.TargetType == right.TargetType && left.Magnitude == right.Magnitude;
        }

        public static bool operator !=(SpellTarget left, SpellTarget right)
        {
            return !(left == right);
        }
    }

    public class SpellEffect
    {
        public SpellEffect()
        {
            AreaType = AreaType.None;
            AreaMagnitude = 0;
            EffectType = EffectType.None;
            EffectMagnitude = 0;
            EffectTag = "";
            EffectAppearance = new DisplayCharacter();
            EffectDescription = "";
        }

        public SpellEffect(SpellEffect spellEffect)
        {
            AreaType = spellEffect.AreaType;
            AreaMagnitude = spellEffect.AreaMagnitude;
            EffectType = spellEffect.EffectType;
            EffectMagnitude = spellEffect.EffectMagnitude;
            EffectTag = spellEffect.EffectTag;
            EffectAppearance = new DisplayCharacter(spellEffect.EffectAppearance);
            EffectDescription = spellEffect.EffectDescription;
        }

        public AreaType AreaType;

        public int AreaMagnitude;

        public EffectType EffectType;

        public int EffectMagnitude;

        public string EffectTag;

        public DisplayCharacter EffectAppearance;

        public string EffectDescription;

        public static bool operator ==(SpellEffect left, SpellEffect right)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(left, right))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)left == null) || ((object)right == null))
            {
                return false;
            }

            // Return true if the fields match:
            return left.AreaType == right.AreaType && left.AreaMagnitude == right.AreaMagnitude && left.EffectType == right.EffectType
                && left.EffectMagnitude == right.EffectMagnitude && left.EffectTag == right.EffectTag && left.EffectAppearance == right.EffectAppearance
                && left.EffectDescription == right.EffectDescription;
        }

        public static bool operator !=(SpellEffect left, SpellEffect right)
        {
            return !(left == right);
        }
    }
}
