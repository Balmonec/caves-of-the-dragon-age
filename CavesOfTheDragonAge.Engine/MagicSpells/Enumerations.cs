﻿using CavesOfTheDragonAge.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Engine.MagicSpells
{
    public enum TargetType
    {
        None,
        Self,
        Adjacent,
        LineOfSight
    }

    public enum AreaType
    {
        None,
        Single,
        Cone,
        Explosion,
        Line,
        //Bolt,
        Level
    }

    public enum EffectType
    {
        None,
        Damage,
        Heal,
        Teleport,
        Trigger
    }

    public static class AreaTypeExtensions
    {
        public static List<Location> GetAreaLocations(this AreaType areaType, Location targetLocation, int magnitude = 0, Location casterLocation = null)
        {
            if(casterLocation == null)
            {
                casterLocation = targetLocation;
            }

            List<Location> areaLocations = new List<Location>();

            switch (areaType)
            {
                case AreaType.None:
                case AreaType.Single:
                    areaLocations.Add(targetLocation);
                    break;

                case AreaType.Explosion:
                    {
                        if (magnitude < 0)
                        {
                            magnitude = 0;
                        }
                        Tuple<int, int> size = new Tuple<int, int>(magnitude * 2 + 1, magnitude * 2 + 1);
                        DijkstraMap radiusMap = new DijkstraMap(size);
                        bool[,] goal = new bool[size.Item1, size.Item2];
                        goal[magnitude, magnitude] = true;
                        radiusMap.BuildMap(goal);

                        for (int x = 0; x < size.Item1; x++)
                        {
                            for (int y = 0; y < size.Item2; y++)
                            {
                                if ((radiusMap.GoalMap(x, y) < (magnitude + 1) * 10)
                                    && (x != magnitude || y != magnitude || targetLocation != casterLocation))
                                {
                                    areaLocations.Add(new Location(targetLocation.X + x - magnitude, targetLocation.Y + y - magnitude));
                                }
                            }
                        }
                    }
                    break;

                case AreaType.Cone:
                    {
                        if (magnitude < 0)
                        {
                            magnitude = 0;
                        }
                        Direction coneDirection = casterLocation.GetDirectionTowardsClockwise(targetLocation);
                        Location coneStart = new Location(magnitude, magnitude).GetVector(coneDirection.Opposite());
                        Tuple<int, int> size = new Tuple<int, int>(magnitude * 2 + 1, magnitude * 2 + 1);
                        DijkstraMap radiusMap = new DijkstraMap(size);
                        bool[,] goal = new bool[size.Item1, size.Item2];
                        goal[magnitude, magnitude] = true;
                        radiusMap.BuildMap(goal);

                        for (int x = 0; x < size.Item1; x++)
                        {
                            for (int y = 0; y < size.Item2; y++)
                            {
                                if ((radiusMap.GoalMap(x, y) < (magnitude + 1) * 10)
                                    && (x != magnitude || y != magnitude || targetLocation != casterLocation)
                                    && (coneStart.GetDirectionTowardsClockwise(x, y) == coneDirection || coneStart.GetDirectionTowardsCounterclockwise(x, y) == coneDirection))
                                {
                                    areaLocations.Add(new Location(targetLocation.X + x - magnitude, targetLocation.Y + y - magnitude));
                                }
                            }
                        }
                    }
                    break;

                case AreaType.Line:
                    {
                        Direction lineDirection = casterLocation.GetDirectionTowardsClockwise(targetLocation);
                        for(int i = 1; i<=magnitude; i++)
                        {
                            Location nextLocation = new Location(casterLocation);
                            for(int len = 0; len < i; len++)
                            {
                                nextLocation = nextLocation.GetVector(lineDirection);
                            }
                            if (nextLocation != casterLocation)
                            {
                                areaLocations.Add(nextLocation);
                            }
                        }
                    }
                    break;

                case AreaType.Level:
                    {
                        int width = GameData.DungeonMaps[GameData.CurrentDungeon].Width;
                        int height = GameData.DungeonMaps[GameData.CurrentDungeon].Height;

                        for (int x = 0; x < width; x++)
                        {
                            for (int y = 0; y < height; y++)
                            {
                                areaLocations.Add(new Location(x, y));
                            }
                        }
                    }
                    break;

                default:
                    break;
            }

            return areaLocations;
        }
    }
}
