﻿using CavesOfTheDragonAge.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Engine.MagicSpells
{
    public static partial class MagicSpellFactory
    {
        /// <summary>
        /// Get a new magic spell.
        /// </summary>
        /// <param name="type">The type of spell to generate.</param>
        /// <param name="name">The name of the new spell.</param>
        /// <returns></returns>
        public static MagicSpell GetNewMagicSpell(string name, TargetRune targetRune, AreaRune areaRune, EffectRune effectRune)
        {
            MagicSpell spell = new MagicSpell(name);
            spell.Target = targetRune.SpellTarget;
            spell.EffectList = new List<SpellEffect>();
            foreach(SpellEffect effect in effectRune.Effects)
            {
                spell.EffectList.Add(new SpellEffect
                {
                    AreaType = areaRune.AreaType,
                    AreaMagnitude = areaRune.AreaMagnitude,
                    EffectType = effect.EffectType,
                    EffectMagnitude = effect.EffectMagnitude,
                    EffectTag = effect.EffectTag,
                    EffectAppearance = effect.EffectAppearance,
                    EffectDescription = effect.EffectDescription
                });
            }

            spell.TargetRune = targetRune;
            spell.AreaRune = areaRune;
            spell.EffectRune = effectRune;

            return spell;
        }

        /// <summary>
        /// Get a random magic spell from a stock list.
        /// </summary>
        /// <returns>A random magic spell from a stock list.</returns>
        public static MagicSpell GetRandomStockSpell()
        {
            List<MagicSpell> stockSpells = new List<MagicSpell>()
            {
                GetNewMagicSpell("Mass Heal", MagicRuneFactory.GetNewTargetRune(TargetType.Self), MagicRuneFactory.GetNewAreaRune("LARGEEXPLOSION"), MagicRuneFactory.GetNewEffectRune("HEAL")),
                GetNewMagicSpell("Mass Cure", MagicRuneFactory.GetNewTargetRune(TargetType.Adjacent), MagicRuneFactory.GetNewAreaRune("LARGECONE"), MagicRuneFactory.GetNewEffectRune("HEAL")),
                GetNewMagicSpell("Fireball", MagicRuneFactory.GetNewTargetRune(TargetType.LineOfSight), MagicRuneFactory.GetNewAreaRune("SMALLEXPLOSION"), MagicRuneFactory.GetNewEffectRune("FIRE")),
                GetNewMagicSpell("Ice Vortex", MagicRuneFactory.GetNewTargetRune(TargetType.Self), MagicRuneFactory.GetNewAreaRune("MEDIUMEXPLOSION"), MagicRuneFactory.GetNewEffectRune("ICE")),
                GetNewMagicSpell("Flee The Scene", MagicRuneFactory.GetNewTargetRune(TargetType.Adjacent), MagicRuneFactory.GetNewAreaRune("HUGECONE"), MagicRuneFactory.GetNewEffectRune("TELEPORT")),
                GetNewMagicSpell("Energy Wave", MagicRuneFactory.GetNewTargetRune(TargetType.Adjacent), MagicRuneFactory.GetNewAreaRune("MEDIUMCONE"), MagicRuneFactory.GetNewEffectRune("FORCE")),
                GetNewMagicSpell("Lightning Bolt", MagicRuneFactory.GetNewTargetRune(TargetType.Adjacent), MagicRuneFactory.GetNewAreaRune("LONGLINE"), MagicRuneFactory.GetNewEffectRune("LIGHTNING")),
                //GetNewMagicSpell("Level Buster", MagicRuneFactory.GetNewTargetRune(TargetType.Self), MagicRuneFactory.GetNewAreaRune("LEVEL"), MagicRuneFactory.GetNewEffectRune("FIRE")),
                GetNewMagicSpell("Fiery Fist", MagicRuneFactory.GetNewTargetRune(TargetType.Adjacent), MagicRuneFactory.GetNewAreaRune("SINGLE"), MagicRuneFactory.GetNewEffectRune("FIRE")),
                GetNewMagicSpell("Random Teleport", MagicRuneFactory.GetNewTargetRune(TargetType.Self), MagicRuneFactory.GetNewAreaRune("HUGEEXPLOSION"), MagicRuneFactory.GetNewEffectRune("TELEPORT")),
                GetNewMagicSpell("Self Immolation", MagicRuneFactory.GetNewTargetRune(TargetType.Self), MagicRuneFactory.GetNewAreaRune("SINGLE"), MagicRuneFactory.GetNewEffectRune("FIRE")),
                GetNewMagicSpell("Blink", MagicRuneFactory.GetNewTargetRune(TargetType.Adjacent), MagicRuneFactory.GetNewAreaRune("SMALLCONE"), MagicRuneFactory.GetNewEffectRune("TELEPORT")),
                GetNewMagicSpell("Mass Open", MagicRuneFactory.GetNewTargetRune(TargetType.Self), MagicRuneFactory.GetNewAreaRune("MEDIUMEXPLOSION"), MagicRuneFactory.GetNewEffectRune("PRY")),
                GetNewMagicSpell("Mass Close", MagicRuneFactory.GetNewTargetRune(TargetType.Self), MagicRuneFactory.GetNewAreaRune("MEDIUMEXPLOSION"), MagicRuneFactory.GetNewEffectRune("SLAM")),
                GetNewMagicSpell("Crevasse", MagicRuneFactory.GetNewTargetRune(TargetType.Adjacent), MagicRuneFactory.GetNewAreaRune("MEDIUMLINE"), MagicRuneFactory.GetNewEffectRune("EARTH")),
                GetNewMagicSpell("Shatter", MagicRuneFactory.GetNewTargetRune(TargetType.Adjacent), MagicRuneFactory.GetNewAreaRune("SMALLEXPLOSION"), MagicRuneFactory.GetNewEffectRune("EARTH")),
                GetNewMagicSpell("Distant Stone", MagicRuneFactory.GetNewTargetRune(TargetType.LineOfSight), MagicRuneFactory.GetNewAreaRune("SINGLE"), MagicRuneFactory.GetNewEffectRune("DIG")),
                GetNewMagicSpell("Earth Shock", MagicRuneFactory.GetNewTargetRune(TargetType.Self), MagicRuneFactory.GetNewAreaRune("LARGEEXPLOSION"), MagicRuneFactory.GetNewEffectRune("DIG")),
            };

            //return stockSpells[2];

            return stockSpells[Randomization.RandomInt(0, stockSpells.Count() - 1, true)];
        }

        //private static MagicSpell getHealingSpell(string name)
        //{
        //    MagicSpell spell = new MagicSpell(name);
        //    spell.Target = new SpellTarget { TargetType = TargetType.Self, Magnitude = 0 };
        //    spell.EffectList = new List<SpellEffect>();
        //    spell.EffectList.Add(new SpellEffect
        //    {
        //        AreaType = AreaType.Single,
        //        AreaMagnitude = 0,
        //        EffectType = EffectType.Heal,
        //        EffectMagnitude = 5,
        //        EffectTag = "",
        //        EffectAppearance = new DisplayCharacter('#', GameColors.Azure),
        //        EffectDescription = "{0} heals {1} hit points."
        //    });

        //    return spell;
        //}

        //private static MagicSpell getFireVortexSpell(string name)
        //{
        //    MagicSpell spell = new MagicSpell(name);
        //    spell.Target = new SpellTarget { TargetType = TargetType.LineOfSight, Magnitude = 0 };
        //    spell.EffectList = new List<SpellEffect>();
        //    spell.EffectList.Add(new SpellEffect
        //    {
        //        AreaType = AreaType.Explosion,
        //        AreaMagnitude = 1,
        //        EffectType = EffectType.Damage,
        //        EffectMagnitude = 5,
        //        EffectTag = "",
        //        EffectAppearance = new DisplayCharacter('*', GameColors.Red),
        //        EffectDescription = "{0} is burned for {1} hit points."
        //    });
        //    spell.EffectList.Add(new SpellEffect
        //    {
        //        AreaType = AreaType.Explosion,
        //        AreaMagnitude = 1,
        //        EffectType = EffectType.Trigger,
        //        EffectMagnitude = 0,
        //        EffectTag = "BURN",
        //        EffectAppearance = null,
        //        EffectDescription = ""
        //    });

        //    return spell;
        //}

        //private static MagicSpell getIceVortexSpell(string name)
        //{
        //    MagicSpell spell = new MagicSpell(name);
        //    spell.Target = new SpellTarget { TargetType = TargetType.Self, Magnitude = 0 };
        //    spell.EffectList = new List<SpellEffect>();
        //    spell.EffectList.Add(new SpellEffect
        //    {
        //        AreaType = AreaType.Explosion,
        //        AreaMagnitude = 3,
        //        EffectType = EffectType.Damage,
        //        EffectMagnitude = 2,
        //        EffectTag = "",
        //        EffectAppearance = new DisplayCharacter('*', System.Drawing.Color.White),
        //        EffectDescription = "{0} is frozen for {1} hit points."
        //    });
        //    spell.EffectList.Add(new SpellEffect
        //    {
        //        AreaType = AreaType.Explosion,
        //        AreaMagnitude = 3,
        //        EffectType = EffectType.Trigger,
        //        EffectMagnitude = 0,
        //        EffectTag = "FREEZE",
        //        EffectAppearance = null,
        //        EffectDescription = ""
        //    });

        //    return spell;
        //}

        //private static MagicSpell getBlinkSpell(string name)
        //{
        //    MagicSpell spell = new MagicSpell(name);
        //    spell.Target = new SpellTarget { TargetType = TargetType.Adjacent, Magnitude = 0 };
        //    spell.EffectList = new List<SpellEffect>();
        //    spell.EffectList.Add(new SpellEffect
        //    {
        //        AreaType = AreaType.Cone,
        //        AreaMagnitude = 10,
        //        EffectType = EffectType.Teleport,
        //        EffectMagnitude = 0,
        //        EffectTag = "Destination Blocked",
        //        EffectAppearance = new DisplayCharacter('#', System.Drawing.Color.Purple),
        //        EffectDescription = "{0} teleports in a cloud of smoke!"
        //    });

        //    return spell;
        //}

        //private static MagicSpell getEnergyWaveSpell(string name)
        //{
        //    MagicSpell spell = new MagicSpell(name);
        //    spell.Target = new SpellTarget { TargetType = TargetType.LineOfSight, Magnitude = 0 };
        //    spell.EffectList = new List<SpellEffect>();
        //    spell.EffectList.Add(new SpellEffect
        //    {
        //        AreaType = AreaType.Cone,
        //        AreaMagnitude = 4,
        //        EffectType = EffectType.Damage,
        //        EffectMagnitude = 4,
        //        EffectTag = "",
        //        EffectAppearance = new DisplayCharacter('~', GameColors.LightBlue),
        //        EffectDescription = "{0} is shocked for {1} hit points."
        //    });

        //    return spell;
        //}

        //private static MagicSpell getIceBoltSpell(string name)
        //{
        //    MagicSpell spell = new MagicSpell(name);
        //    spell.Target = new SpellTarget { TargetType = TargetType.LineOfSight, Magnitude = 0 };
        //    spell.EffectList = new List<SpellEffect>();
        //    spell.EffectList.Add(new SpellEffect
        //    {
        //        AreaType = AreaType.Line,
        //        AreaMagnitude = 10,
        //        EffectType = EffectType.Damage,
        //        EffectMagnitude = 6,
        //        EffectTag = "",
        //        EffectAppearance = new DisplayCharacter('*', System.Drawing.Color.White),
        //        EffectDescription = "{0} is frozen for {1} hit points."
        //    });
        //    spell.EffectList.Add(new SpellEffect
        //    {
        //        AreaType = AreaType.Line,
        //        AreaMagnitude = 10,
        //        EffectType = EffectType.Trigger,
        //        EffectMagnitude = 0,
        //        EffectTag = "FREEZE",
        //        EffectAppearance = null,
        //        EffectDescription = ""
        //    });

        //    return spell;
        //}

        //private static MagicSpell getLevelBusterSpell(string name)
        //{
        //    MagicSpell spell = new MagicSpell(name);
        //    spell.Target = new SpellTarget { TargetType = TargetType.Self, Magnitude = 0 };
        //    spell.EffectList = new List<SpellEffect>();
        //    spell.EffectList.Add(new SpellEffect
        //    {
        //        AreaType = AreaType.Level,
        //        AreaMagnitude = 0,
        //        EffectType = EffectType.Damage,
        //        EffectMagnitude = 5,
        //        EffectTag = "",
        //        EffectAppearance = new DisplayCharacter('*', GameColors.Red),
        //        EffectDescription = "{0} is burned for {1} hit points."
        //    });
        //    //spell.EffectList.Add(new SpellEffect
        //    //{
        //    //    AreaType = AreaType.Level,
        //    //    AreaMagnitude = 0,
        //    //    EffectType = EffectType.Trigger,
        //    //    EffectMagnitude = 0,
        //    //    EffectTag = "BURN",
        //    //    EffectAppearance = null,
        //    //    EffectDescription = ""
        //    //});

        //    return spell;
        //}
    }
}
