﻿using CavesOfTheDragonAge.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CavesOfTheDragonAgeTests
{
    /// <summary>
    ///This is a test class for DirectionExtensionsTest and is intended
    ///to contain all DirectionExtensionsTest Unit Tests
    ///</summary>
    [TestClass]
    public class DirectionExtensionsTest
    {
        /// <summary>
        ///Test GetDeltaCoordinates completely.
        ///</summary>
        [TestMethod]
        public void GetDeltaCoordinatesTest()
        {
            Tuple<Direction, Tuple<int, int>>[] sampleData = { new Tuple<Direction, Tuple<int, int>>(Direction.Undefined, new Tuple<int, int>(0, 0)),
                                                               new Tuple<Direction, Tuple<int, int>>(Direction.Here, new Tuple<int, int>(0, 0)),
                                                               new Tuple<Direction, Tuple<int, int>>(Direction.North, new Tuple<int, int>(0, -1)),
                                                               new Tuple<Direction, Tuple<int, int>>(Direction.South, new Tuple<int, int>(0, 1)),
                                                               new Tuple<Direction, Tuple<int, int>>(Direction.East, new Tuple<int, int>(1, 0)),
                                                               new Tuple<Direction, Tuple<int, int>>(Direction.West, new Tuple<int, int>(-1, 0)),
                                                               new Tuple<Direction, Tuple<int, int>>(Direction.NorthEast, new Tuple<int, int>(1, -1)),
                                                               new Tuple<Direction, Tuple<int, int>>(Direction.NorthWest, new Tuple<int, int>(-1, -1)),
                                                               new Tuple<Direction, Tuple<int, int>>(Direction.SouthEast, new Tuple<int, int>(1, 1)),
                                                               new Tuple<Direction, Tuple<int, int>>(Direction.SouthWest, new Tuple<int, int>(-1, 1)),
                                                               new Tuple<Direction, Tuple<int, int>>(Direction.Up, new Tuple<int, int>(0, 0)),
                                                               new Tuple<Direction, Tuple<int, int>>(Direction.Down, new Tuple<int, int>(0, 0)),
                                                               };
            foreach (Tuple<Direction, Tuple<int, int>> currentData in sampleData)
            {
                Tuple<int, int> expected = currentData.Item2;
                Tuple<int, int> actual = currentData.Item1.GetDeltaCoordinates();
                Assert.AreEqual(expected, actual, "Direction<" + currentData.Item1 + ">");
            }
        }

        /// <summary>
        ///Test GetDeltaX completely.
        ///</summary>
        [TestMethod]
        public void GetDeltaXTest()
        {
            Tuple<Direction, int>[] sampleData = { new Tuple<Direction, int>(Direction.Undefined, 0),
                                                   new Tuple<Direction, int>(Direction.Here, 0),
                                                   new Tuple<Direction, int>(Direction.North, 0),
                                                   new Tuple<Direction, int>(Direction.South, 0),
                                                   new Tuple<Direction, int>(Direction.East, 1),
                                                   new Tuple<Direction, int>(Direction.West, -1),
                                                   new Tuple<Direction, int>(Direction.NorthEast, 1),
                                                   new Tuple<Direction, int>(Direction.NorthWest, -1),
                                                   new Tuple<Direction, int>(Direction.SouthEast, 1),
                                                   new Tuple<Direction, int>(Direction.SouthWest, -1),
                                                   new Tuple<Direction, int>(Direction.Up, 0),
                                                   new Tuple<Direction, int>(Direction.Down, 0),
                                                   };
            foreach (Tuple<Direction, int> currentData in sampleData)
            {
                int expected = currentData.Item2;
                int actual = currentData.Item1.GetDeltaX();
                Assert.AreEqual(expected, actual, "Direction<" + currentData.Item1 + ">");
            }
        }

        /// <summary>
        ///Test GetDeltaY completely.
        ///</summary>
        [TestMethod]
        public void GetDeltaYTest()
        {
            Tuple<Direction, int>[] sampleData = { new Tuple<Direction, int>(Direction.Undefined, 0),
                                                   new Tuple<Direction, int>(Direction.Here, 0),
                                                   new Tuple<Direction, int>(Direction.North, -1),
                                                   new Tuple<Direction, int>(Direction.South, 1),
                                                   new Tuple<Direction, int>(Direction.East, 0),
                                                   new Tuple<Direction, int>(Direction.West, 0),
                                                   new Tuple<Direction, int>(Direction.NorthEast, -1),
                                                   new Tuple<Direction, int>(Direction.NorthWest, -1),
                                                   new Tuple<Direction, int>(Direction.SouthEast, 1),
                                                   new Tuple<Direction, int>(Direction.SouthWest, 1),
                                                   new Tuple<Direction, int>(Direction.Up, 0),
                                                   new Tuple<Direction, int>(Direction.Down, 0),
                                                   };
            foreach (Tuple<Direction, int> currentData in sampleData)
            {
                int expected = currentData.Item2;
                int actual = currentData.Item1.GetDeltaY();
                Assert.AreEqual(expected, actual, "Direction<" + currentData.Item1 + ">");
            }
        }

        /// <summary>
        ///Test IsHorizontalMovement completely.
        ///</summary>
        [TestMethod]
        public void IsHorizontalMovementTest()
        {
            Tuple<Direction, bool>[] sampleData = { new Tuple<Direction, bool>(Direction.Undefined, false),
                                                    new Tuple<Direction, bool>(Direction.Here, false),
                                                    new Tuple<Direction, bool>(Direction.North, true),
                                                    new Tuple<Direction, bool>(Direction.South, true),
                                                    new Tuple<Direction, bool>(Direction.East, true),
                                                    new Tuple<Direction, bool>(Direction.West, true),
                                                    new Tuple<Direction, bool>(Direction.NorthEast, true),
                                                    new Tuple<Direction, bool>(Direction.NorthWest, true),
                                                    new Tuple<Direction, bool>(Direction.SouthEast, true),
                                                    new Tuple<Direction, bool>(Direction.SouthWest, true),
                                                    new Tuple<Direction, bool>(Direction.Up, false),
                                                    new Tuple<Direction, bool>(Direction.Down, false),
                                                    };
            foreach (Tuple<Direction, bool> currentData in sampleData)
            {
                bool expected = currentData.Item2;
                bool actual = currentData.Item1.IsHorizontalMovement();
                Assert.AreEqual(expected, actual, "Direction<" + currentData.Item1 + ">");
            }
        }

        /// <summary>
        ///Test IsOrthogonalMovement completely.
        ///</summary>
        [TestMethod]
        public void IsOrthogonalMovementTest()
        {
            Tuple<Direction, bool>[] sampleData = { new Tuple<Direction, bool>(Direction.Undefined, false),
                                                    new Tuple<Direction, bool>(Direction.Here, false),
                                                    new Tuple<Direction, bool>(Direction.North, true),
                                                    new Tuple<Direction, bool>(Direction.South, true),
                                                    new Tuple<Direction, bool>(Direction.East, true),
                                                    new Tuple<Direction, bool>(Direction.West, true),
                                                    new Tuple<Direction, bool>(Direction.NorthEast, false),
                                                    new Tuple<Direction, bool>(Direction.NorthWest, false),
                                                    new Tuple<Direction, bool>(Direction.SouthEast, false),
                                                    new Tuple<Direction, bool>(Direction.SouthWest, false),
                                                    new Tuple<Direction, bool>(Direction.Up, false),
                                                    new Tuple<Direction, bool>(Direction.Down, false),
                                                    };
            foreach (Tuple<Direction, bool> currentData in sampleData)
            {
                bool expected = currentData.Item2;
                bool actual = currentData.Item1.IsOrthogonalMovement();
                Assert.AreEqual(expected, actual, "Direction<" + currentData.Item1 + ">");
            }
        }

        /// <summary>
        ///Test IsDiagonalMovement completely.
        ///</summary>
        [TestMethod]
        public void IsDiagonalMovementTest()
        {
            Tuple<Direction, bool>[] sampleData = { new Tuple<Direction, bool>(Direction.Undefined, false),
                                                    new Tuple<Direction, bool>(Direction.Here, false),
                                                    new Tuple<Direction, bool>(Direction.North, false),
                                                    new Tuple<Direction, bool>(Direction.South, false),
                                                    new Tuple<Direction, bool>(Direction.East, false),
                                                    new Tuple<Direction, bool>(Direction.West, false),
                                                    new Tuple<Direction, bool>(Direction.NorthEast, true),
                                                    new Tuple<Direction, bool>(Direction.NorthWest, true),
                                                    new Tuple<Direction, bool>(Direction.SouthEast, true),
                                                    new Tuple<Direction, bool>(Direction.SouthWest, true),
                                                    new Tuple<Direction, bool>(Direction.Up, false),
                                                    new Tuple<Direction, bool>(Direction.Down, false),
                                                    };
            foreach (Tuple<Direction, bool> currentData in sampleData)
            {
                bool expected = currentData.Item2;
                bool actual = currentData.Item1.IsDiagonalMovement();
                Assert.AreEqual(expected, actual, "Direction<" + currentData.Item1 + ">");
            }
        }

        /// <summary>
        ///A test for Left90
        ///</summary>
        [TestMethod()]
        public void Left90Test()
        {
            Tuple<Direction, Direction>[] sampleData = { new Tuple<Direction, Direction>(Direction.Undefined, Direction.Undefined),
                                                         new Tuple<Direction, Direction>(Direction.Here, Direction.Undefined),
                                                         new Tuple<Direction, Direction>(Direction.North, Direction.West),
                                                         new Tuple<Direction, Direction>(Direction.South, Direction.East),
                                                         new Tuple<Direction, Direction>(Direction.East, Direction.North),
                                                         new Tuple<Direction, Direction>(Direction.West, Direction.South),
                                                         new Tuple<Direction, Direction>(Direction.NorthEast, Direction.NorthWest),
                                                         new Tuple<Direction, Direction>(Direction.NorthWest, Direction.SouthWest),
                                                         new Tuple<Direction, Direction>(Direction.SouthEast, Direction.NorthEast),
                                                         new Tuple<Direction, Direction>(Direction.SouthWest, Direction.SouthEast),
                                                         new Tuple<Direction, Direction>(Direction.Up, Direction.Undefined),
                                                         new Tuple<Direction, Direction>(Direction.Down, Direction.Undefined),
                                                         };
            foreach (Tuple<Direction, Direction> currentData in sampleData)
            {
                Direction expected = currentData.Item2;
                Direction actual = currentData.Item1.Left90();
                Assert.AreEqual(expected, actual, "Direction<" + currentData.Item1 + ">");
            }
        }

        /// <summary>
        ///A test for Left45
        ///</summary>
        [TestMethod()]
        public void Left45Test()
        {
            Tuple<Direction, Direction>[] sampleData = { new Tuple<Direction, Direction>(Direction.Undefined, Direction.Undefined),
                                                         new Tuple<Direction, Direction>(Direction.Here, Direction.Undefined),
                                                         new Tuple<Direction, Direction>(Direction.North, Direction.NorthWest),
                                                         new Tuple<Direction, Direction>(Direction.South, Direction.SouthEast),
                                                         new Tuple<Direction, Direction>(Direction.East, Direction.NorthEast),
                                                         new Tuple<Direction, Direction>(Direction.West, Direction.SouthWest),
                                                         new Tuple<Direction, Direction>(Direction.NorthEast, Direction.North),
                                                         new Tuple<Direction, Direction>(Direction.NorthWest, Direction.West),
                                                         new Tuple<Direction, Direction>(Direction.SouthEast, Direction.East),
                                                         new Tuple<Direction, Direction>(Direction.SouthWest, Direction.South),
                                                         new Tuple<Direction, Direction>(Direction.Up, Direction.Undefined),
                                                         new Tuple<Direction, Direction>(Direction.Down, Direction.Undefined),
                                                         };
            foreach (Tuple<Direction, Direction> currentData in sampleData)
            {
                Direction expected = currentData.Item2;
                Direction actual = currentData.Item1.Left45();
                Assert.AreEqual(expected, actual, "Direction<" + currentData.Item1 + ">");
            }
        }

        /// <summary>
        ///A test for Opposite
        ///</summary>
        [TestMethod()]
        public void OppositeTest()
        {
            Tuple<Direction, Direction>[] sampleData = { new Tuple<Direction, Direction>(Direction.Undefined, Direction.Undefined),
                                                         new Tuple<Direction, Direction>(Direction.Here, Direction.Undefined),
                                                         new Tuple<Direction, Direction>(Direction.North, Direction.South),
                                                         new Tuple<Direction, Direction>(Direction.South, Direction.North),
                                                         new Tuple<Direction, Direction>(Direction.East, Direction.West),
                                                         new Tuple<Direction, Direction>(Direction.West, Direction.East),
                                                         new Tuple<Direction, Direction>(Direction.NorthEast, Direction.SouthWest),
                                                         new Tuple<Direction, Direction>(Direction.NorthWest, Direction.SouthEast),
                                                         new Tuple<Direction, Direction>(Direction.SouthEast, Direction.NorthWest),
                                                         new Tuple<Direction, Direction>(Direction.SouthWest, Direction.NorthEast),
                                                         new Tuple<Direction, Direction>(Direction.Up, Direction.Down),
                                                         new Tuple<Direction, Direction>(Direction.Down, Direction.Up),
                                                         };
            foreach (Tuple<Direction, Direction> currentData in sampleData)
            {
                Direction expected = currentData.Item2;
                Direction actual = currentData.Item1.Opposite();
                Assert.AreEqual(expected, actual, "Direction<" + currentData.Item1 + ">");
            }
        }

        /// <summary>
        ///A test for Right90
        ///</summary>
        [TestMethod()]
        public void Right90Test()
        {
            Tuple<Direction, Direction>[] sampleData = { new Tuple<Direction, Direction>(Direction.Undefined, Direction.Undefined),
                                                         new Tuple<Direction, Direction>(Direction.Here, Direction.Undefined),
                                                         new Tuple<Direction, Direction>(Direction.North, Direction.East),
                                                         new Tuple<Direction, Direction>(Direction.South, Direction.West),
                                                         new Tuple<Direction, Direction>(Direction.East, Direction.South),
                                                         new Tuple<Direction, Direction>(Direction.West, Direction.North),
                                                         new Tuple<Direction, Direction>(Direction.NorthEast, Direction.SouthEast),
                                                         new Tuple<Direction, Direction>(Direction.NorthWest, Direction.NorthEast),
                                                         new Tuple<Direction, Direction>(Direction.SouthEast, Direction.SouthWest),
                                                         new Tuple<Direction, Direction>(Direction.SouthWest, Direction.NorthWest),
                                                         new Tuple<Direction, Direction>(Direction.Up, Direction.Undefined),
                                                         new Tuple<Direction, Direction>(Direction.Down, Direction.Undefined),
                                                         };
            foreach (Tuple<Direction, Direction> currentData in sampleData)
            {
                Direction expected = currentData.Item2;
                Direction actual = currentData.Item1.Right90();
                Assert.AreEqual(expected, actual, "Direction<" + currentData.Item1 + ">");
            }
        }

        /// <summary>
        ///A test for Right45
        ///</summary>
        [TestMethod()]
        public void Right45Test()
        {
            Tuple<Direction, Direction>[] sampleData = { new Tuple<Direction, Direction>(Direction.Undefined, Direction.Undefined),
                                                         new Tuple<Direction, Direction>(Direction.Here, Direction.Undefined),
                                                         new Tuple<Direction, Direction>(Direction.North, Direction.NorthEast),
                                                         new Tuple<Direction, Direction>(Direction.South, Direction.SouthWest),
                                                         new Tuple<Direction, Direction>(Direction.East, Direction.SouthEast),
                                                         new Tuple<Direction, Direction>(Direction.West, Direction.NorthWest),
                                                         new Tuple<Direction, Direction>(Direction.NorthEast, Direction.East),
                                                         new Tuple<Direction, Direction>(Direction.NorthWest, Direction.North),
                                                         new Tuple<Direction, Direction>(Direction.SouthEast, Direction.South),
                                                         new Tuple<Direction, Direction>(Direction.SouthWest, Direction.West),
                                                         new Tuple<Direction, Direction>(Direction.Up, Direction.Undefined),
                                                         new Tuple<Direction, Direction>(Direction.Down, Direction.Undefined),
                                                         };
            foreach (Tuple<Direction, Direction> currentData in sampleData)
            {
                Direction expected = currentData.Item2;
                Direction actual = currentData.Item1.Right45();
                Assert.AreEqual(expected, actual, "Direction<" + currentData.Item1 + ">");
            }
        }

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        //
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion Additional test attributes
    }
}