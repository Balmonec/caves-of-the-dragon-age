﻿using CavesOfTheDragonAge.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing;

namespace CavesOfTheDragonAgeTests
{
    
    
    /// <summary>
    ///This is a test class for DisplayCharacterTest and is intended
    ///to contain all DisplayCharacterTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DisplayCharacterTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for DisplayCharacter Constructor
        ///</summary>
        [TestMethod()]
        public void DisplayCharacterConstructorTest()
        {
            DisplayCharacter target = new DisplayCharacter();
            Assert.AreEqual((target != null), true);
        }

        /// <summary>
        ///A test for DisplayCharacter Constructor
        ///</summary>
        [TestMethod()]
        public void DisplayCharacterConstructorTest1()
        {
            var sampleData = new[]
            {
                new { Character = '#', ForegroundColor = GameColors.Blue},
                new { Character = ';', ForegroundColor = GameColors.Azure},
                new { Character = 'g', ForegroundColor = GameColors.Blue},
                new { Character = '#', ForegroundColor = GameColors.Brown},
                new { Character = '6', ForegroundColor = GameColors.Blue},
                new { Character = '#', ForegroundColor = GameColors.LightBlue},
            };
            foreach (var item in sampleData)
            {
                DisplayCharacter expected = new DisplayCharacter();
                expected.Character = item.Character;
                expected.ForegroundColor = item.ForegroundColor;

                DisplayCharacter actual = new DisplayCharacter(item.Character, item.ForegroundColor);

                Assert.AreEqual((expected == actual), true, "Character<" + item.Character + ">. ForegroundColor<" + item.ForegroundColor + ">");
            }
        }

        /// <summary>
        ///A test for DisplayCharacter Constructor
        ///</summary>
        [TestMethod()]
        public void DisplayCharacterConstructorTest2()
        {
            var sampleData = new[]
            {
                new { Character = '#', ForegroundColor = GameColors.Blue, BackgroundColor = GameColors.Blue},
                new { Character = ';', ForegroundColor = GameColors.Azure, BackgroundColor = GameColors.Blue},
                new { Character = 'g', ForegroundColor = GameColors.Blue, BackgroundColor = GameColors.Blue},
                new { Character = '#', ForegroundColor = GameColors.Brown, BackgroundColor = GameColors.Blue},
                new { Character = '6', ForegroundColor = GameColors.Blue, BackgroundColor = GameColors.Blue},
                new { Character = '#', ForegroundColor = GameColors.LightBlue, BackgroundColor = GameColors.Blue},
                new { Character = ';', ForegroundColor = GameColors.Azure, BackgroundColor = GameColors.Azure},
                new { Character = 'g', ForegroundColor = GameColors.Blue, BackgroundColor = GameColors.Brown},
                new { Character = '#', ForegroundColor = GameColors.Brown, BackgroundColor = GameColors.LightBlue},
                new { Character = '6', ForegroundColor = GameColors.Blue, BackgroundColor = GameColors.SaddleBrown},
            };
            foreach (var item in sampleData)
            {
                DisplayCharacter expected = new DisplayCharacter();
                expected.Character = item.Character;
                expected.ForegroundColor = item.ForegroundColor;
                expected.BackgroundColor = item.BackgroundColor;

                DisplayCharacter actual = new DisplayCharacter(item.Character, item.ForegroundColor, item.BackgroundColor);

                Assert.AreEqual((expected == actual), true, "Character<" + item.Character + ">. ForegroundColor<" + item.ForegroundColor + ">. BackgroundColor<" + item.BackgroundColor + ">");
            }
        }

        /// <summary>
        ///A test for op_Equality
        ///</summary>
        [TestMethod()]
        public void op_EqualityTest()
        {
            var sampleData = new[]
            {
                new { Left = new DisplayCharacter('#', GameColors.Blue, GameColors.Blue), Right = new DisplayCharacter('#', GameColors.Blue, GameColors.Blue), Expected = true},
                new { Left = new DisplayCharacter('#', GameColors.Blue, GameColors.Blue), Right = new DisplayCharacter('#', GameColors.Azure, GameColors.Blue), Expected = false},
                new { Left = new DisplayCharacter('#', GameColors.Blue, GameColors.Blue), Right = new DisplayCharacter(';', GameColors.Blue, GameColors.Blue), Expected = false},
                new { Left = new DisplayCharacter('#', GameColors.Blue, GameColors.Blue), Right = new DisplayCharacter('#', GameColors.Blue, GameColors.Azure), Expected = false},
            };
            foreach (var item in sampleData)
            {
                bool expected = item.Expected;
                bool actual = (item.Left == item.Right);
                Assert.AreEqual(expected, actual, "Left<" + item.Left.Character + ", " + item.Left.ForegroundColor + ", " + item.Left.BackgroundColor + ">"
                    + ". Right<" + item.Right.Character + ", " + item.Right.ForegroundColor + ", " + item.Right.BackgroundColor + ">");
            }
        }

        /// <summary>
        ///A test for op_Inequality
        ///</summary>
        [TestMethod()]
        public void op_InequalityTest()
        {
            var sampleData = new[]
            {
                new { Left = new DisplayCharacter('#', GameColors.Blue, GameColors.Blue), Right = new DisplayCharacter('#', GameColors.Blue, GameColors.Blue), Expected = false},
                new { Left = new DisplayCharacter('#', GameColors.Blue, GameColors.Blue), Right = new DisplayCharacter('#', GameColors.Azure, GameColors.Blue), Expected = true},
                new { Left = new DisplayCharacter('#', GameColors.Blue, GameColors.Blue), Right = new DisplayCharacter(';', GameColors.Blue, GameColors.Blue), Expected = true},
                new { Left = new DisplayCharacter('#', GameColors.Blue, GameColors.Blue), Right = new DisplayCharacter('#', GameColors.Blue, GameColors.Azure), Expected = true},
            };
            foreach (var item in sampleData)
            {
                bool expected = item.Expected;
                bool actual = (item.Left != item.Right);
                Assert.AreEqual(expected, actual);
            }
        }

        /// <summary>
        ///A test for BackgroundColor
        ///</summary>
        [TestMethod()]
        public void BackgroundColorTest()
        {
            var sampleData = new[]
            {
                new { Color = GameColors.Blue },
                new { Color = GameColors.Azure },
                new { Color = GameColors.Brown },
                new { Color = GameColors.LightBlue },
            };
            foreach (var item in sampleData)
            {
                DisplayCharacter target = new DisplayCharacter();
                Color expected = item.Color;
                Color actual;
                target.BackgroundColor = expected;
                actual = target.BackgroundColor;
                Assert.AreEqual(expected, actual);
            }
        }

        /// <summary>
        ///A test for Character
        ///</summary>
        [TestMethod()]
        public void CharacterTest()
        {
            var sampleData = new[]
            {
                new { Character = '#' },
                new { Character = ';' },
                new { Character = 'a' },
                new { Character = '@' },
            };
            foreach (var item in sampleData)
            {
                DisplayCharacter target = new DisplayCharacter();
                char expected = item.Character;
                char actual;
                target.Character = expected;
                actual = target.Character;
                Assert.AreEqual(expected, actual);
            }
        }

        /// <summary>
        ///A test for ForegroundColor
        ///</summary>
        [TestMethod()]
        public void ForegroundColorTest()
        {
            var sampleData = new[]
            {
                new { Color = GameColors.Blue },
                new { Color = GameColors.Azure },
                new { Color = GameColors.Brown },
                new { Color = GameColors.LightBlue },
            };
            foreach (var item in sampleData)
            {
                DisplayCharacter target = new DisplayCharacter();
                Color expected = item.Color;
                Color actual;
                target.ForegroundColor = expected;
                actual = target.ForegroundColor;
                Assert.AreEqual(expected, actual);
            }
        }

        /// <summary>
        ///A test for ToString
        ///</summary>
        [TestMethod()]
        public void ToStringTest()
        {
            var sampleData = new[]
            {
                new { Character = '#', ForegroundColor = GameColors.Blue, BackgroundColor = GameColors.Blue},
                new { Character = ';', ForegroundColor = GameColors.Azure, BackgroundColor = GameColors.Blue},
                new { Character = 'g', ForegroundColor = GameColors.Blue, BackgroundColor = GameColors.Blue},
                new { Character = '#', ForegroundColor = GameColors.Brown, BackgroundColor = GameColors.Blue},
                new { Character = '6', ForegroundColor = GameColors.Blue, BackgroundColor = GameColors.Blue},
                new { Character = '#', ForegroundColor = GameColors.LightBlue, BackgroundColor = GameColors.Blue},
                new { Character = ';', ForegroundColor = GameColors.Azure, BackgroundColor = GameColors.Azure},
                new { Character = 'g', ForegroundColor = GameColors.Blue, BackgroundColor = GameColors.Brown},
                new { Character = '#', ForegroundColor = GameColors.Brown, BackgroundColor = GameColors.LightBlue},
                new { Character = '6', ForegroundColor = GameColors.Blue, BackgroundColor = GameColors.SaddleBrown},
            };
            foreach (var item in sampleData)
            {
                DisplayCharacter character = new DisplayCharacter(item.Character, item.ForegroundColor, item.BackgroundColor);
                string expected = "Character(" + item.Character + "), ForegroundColor(" + item.ForegroundColor + "), BackgroundColor(" + item.BackgroundColor + ")";
                string actual = character.ToString();

                Assert.AreEqual(expected, actual);
            }
        }
    }
}
