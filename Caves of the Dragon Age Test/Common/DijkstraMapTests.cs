﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CavesOfTheDragonAge.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Common.Tests
{
    [TestClass()]
    public class DijkstraMapTests
    {
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void BuildMapNullTest()
        {
            DijkstraMap map = new DijkstraMap(5, 5);
            map.BuildMap(null, null);
        }

        [TestMethod()]
        public void BuildMapGoalCellsMismatchTest()
        {
            var sampleData = new[]
            {
                new { MapSize = new Tuple<int, int>(5, 5), ImpassableSize = new Tuple<int, int>(5, 6) },
                new { MapSize = new Tuple<int, int>(5, 5), ImpassableSize = new Tuple<int, int>(6, 5) },
                new { MapSize = new Tuple<int, int>(5, 5), ImpassableSize = new Tuple<int, int>(6, 6) },
                new { MapSize = new Tuple<int, int>(6, 6), ImpassableSize = new Tuple<int, int>(5, 6) },
                new { MapSize = new Tuple<int, int>(6, 6), ImpassableSize = new Tuple<int, int>(6, 5) },
            };
            foreach (var item in sampleData)
            {
                try
                {
                    DijkstraMap map = new DijkstraMap(item.MapSize.Item1, item.MapSize.Item2);
                    map.BuildMap(new bool[item.ImpassableSize.Item1, item.ImpassableSize.Item2], null);
                    Assert.Fail("An ArgumentOutOfRangeException should have been thrown.  MapSize<" + item.MapSize + ">. GoalSize<" + item.ImpassableSize + ">.");
                }
                catch (ArgumentOutOfRangeException e)
                {
                    Assert.AreEqual("The goalCells array must match the size of the map itself.\r\nParameter name: goalCells", e.Message, "MapSize<" + item.MapSize + ">. GoalSize<" + item.ImpassableSize + ">.");
                    Assert.AreEqual("goalCells", e.ParamName, "MapSize<" + item.MapSize + ">. GoalSize<" + item.ImpassableSize + ">.");
                }
                catch (Exception e)
                {
                    Assert.Fail("Unexpected exception of type " + e.GetType() + " caught: " + e.Message + " MapSize < " + item.MapSize + " >. GoalSize< " + item.ImpassableSize + " >.");
                }
            }
        }

        [TestMethod()]
        public void BuildMapImpassableCellsMismatchTest()
        {
            var sampleData = new[]
            {
                new { MapSize = new Tuple<int, int>(5, 5), ImpassableSize = new Tuple<int, int>(5, 6) },
                new { MapSize = new Tuple<int, int>(5, 5), ImpassableSize = new Tuple<int, int>(6, 5) },
                new { MapSize = new Tuple<int, int>(5, 5), ImpassableSize = new Tuple<int, int>(6, 6) },
                new { MapSize = new Tuple<int, int>(6, 6), ImpassableSize = new Tuple<int, int>(5, 6) },
                new { MapSize = new Tuple<int, int>(6, 6), ImpassableSize = new Tuple<int, int>(6, 5) },
            };
            foreach (var item in sampleData)
            {
                try
                {
                    DijkstraMap map = new DijkstraMap(item.MapSize.Item1, item.MapSize.Item2);
                    map.BuildMap(new bool[item.MapSize.Item1, item.MapSize.Item2], new bool[item.ImpassableSize.Item1, item.ImpassableSize.Item2]);
                    Assert.Fail("An ArgumentOutOfRangeException should have been thrown.  MapSize<" + item.MapSize + ">. ImpassableSize<" + item.ImpassableSize + ">.");
                }
                catch (ArgumentOutOfRangeException e)
                {
                    Assert.AreEqual("The impassableCells array must match the size of the map itself.\r\nParameter name: impassableCells", e.Message, "MapSize<" + item.MapSize + ">. ImpassableSize<" + item.ImpassableSize + ">.");
                    Assert.AreEqual("impassableCells", e.ParamName, "MapSize<" + item.MapSize + ">. ImpassableSize<" + item.ImpassableSize + ">.");
                }
                catch (Exception e)
                {
                    Assert.Fail("Unexpected exception of type " + e.GetType() + " caught: " + e.Message + " MapSize < " + item.MapSize + " >. ImpassableSize< " + item.ImpassableSize + " >.");
                }
            }
        }

        [TestMethod()]
        public void BuildMapGoalTest()
        {
            var sampleData = new[]
            {
                new { Identifer = 0,
                      MapSize = new Tuple<int, int>(5, 5),
                      GoalCells = new bool[5, 5] { { false, false, false, false, false },
                                                   { false, false, false, false, false },
                                                   { false, false, true,  false, false },
                                                   { false, false, false, false, false },
                                                   { false, false, false, false, false } },
                      ImpassableCells = new bool[5, 5] { { false, false, false, false, false },
                                                         { false, false, false, false, false },
                                                         { false, false, false, false, false },
                                                         { false, false, false, false, false },
                                                         { false, false, false, false, false } },
                      Expected = new int[5, 5] { { 30, 25, 20, 25, 30 },
                                                 { 25, 15, 10, 15, 25 },
                                                 { 20, 10, 00, 10, 20 },
                                                 { 25, 15, 10, 15, 25 },
                                                 { 30, 25, 20, 25, 30 } } },
                new { Identifer = 1,
                      MapSize = new Tuple<int, int>(5, 5),
                      GoalCells = new bool[5, 5] { { false, false, false, false, false },
                                                   { false, false, false, false, false },
                                                   { false, false, true,  false, false },
                                                   { false, false, false, false, false },
                                                   { false, false, false, false, false } },
                      ImpassableCells = new bool[5, 5] { { false, false, false, false, false },
                                                         { false, true,  false, true,  false },
                                                         { false, true,  false, true,  false },
                                                         { false, true,  true,  true,  false },
                                                         { false, false, false, false, false } },
                      Expected = new int[5, 5] { { 35, 25,           20,           25,           35 },
                                                 { 40, int.MaxValue, 10,           int.MaxValue, 40 },
                                                 { 50, int.MaxValue, 00,           int.MaxValue, 50 },
                                                 { 60, int.MaxValue, int.MaxValue, int.MaxValue, 60 },
                                                 { 70, 75,           85,           75,           70 } } },
            };
            foreach (var item in sampleData)
            {
                DijkstraMap map = new DijkstraMap(item.MapSize.Item1, item.MapSize.Item2);
                map.BuildMap(item.GoalCells, item.ImpassableCells);
                //Assert.AreEqual(item.Expected, map.GoalMap, "Identifier<" + item.Identifer + ">.");
                for (int x = 0; x < item.MapSize.Item1; x++)
                {
                    for (int y = 0; y < item.MapSize.Item2; y++)
                    {
                        Assert.AreEqual(item.Expected[x, y], map.GoalMap(x, y), "Identifier<" + item.Identifer + ">. X<" + x + ">. Y<" + y + ">.");
                    }
                }
            }
        }

        [TestMethod()]
        public void BuildMapFleeTest()
        {
            var sampleData = new[]
            {
                new { Identifer = 0,
                      MapSize = new Tuple<int, int>(5, 5),
                      GoalCells = new bool[5, 5] { { false, false, false, false, false },
                                                   { false, false, false, false, false },
                                                   { false, false, true,  false, false },
                                                   { false, false, false, false, false },
                                                   { false, false, false, false, false } },
                      ImpassableCells = new bool[5, 5] { { false, false, false, false, false },
                                                         { false, false, false, false, false },
                                                         { false, false, false, false, false },
                                                         { false, false, false, false, false },
                                                         { false, false, false, false, false } },
                      Expected = new int[5, 5] { { -36, -30, -24, -30, -36 },
                                                 { -30, -21, -15, -21, -30 },
                                                 { -24, -15, -06, -15, -24 },
                                                 { -30, -21, -15, -21, -30 },
                                                 { -36, -30, -24, -30, -36 } } },
                new { Identifer = 1,
                      MapSize = new Tuple<int, int>(5, 5),
                      GoalCells = new bool[5, 5] { { false, false, false, false, false },
                                                   { false, false, false, false, false },
                                                   { false, false, true,  false, false },
                                                   { false, false, false, false, false },
                                                   { false, false, false, false, false } },
                      ImpassableCells = new bool[5, 5] { { false, false, false, false, false },
                                                         { false, true,  false, true,  false },
                                                         { false, true,  false, true,  false },
                                                         { false, true,  true,  true,  false },
                                                         { false, false, false, false, false } },
                      Expected = new int[5, 5] { { -47, -42,          -32,          -42,          -47 },
                                                 { -57, int.MaxValue, -27,          int.MaxValue, -57 },
                                                 { -67, int.MaxValue, -17,          int.MaxValue, -67 },
                                                 { -77, int.MaxValue, int.MaxValue, int.MaxValue, -77 },
                                                 { -84, -92,          -102,         -92,          -84 } } },
            };
            foreach (var item in sampleData)
            {
                DijkstraMap map = new DijkstraMap(item.MapSize.Item1, item.MapSize.Item2);
                map.BuildMap(item.GoalCells, item.ImpassableCells);
                //Assert.AreEqual(item.Expected, map.GoalMap, "Identifier<" + item.Identifer + ">.");
                for (int x = 0; x < item.MapSize.Item1; x++)
                {
                    for (int y = 0; y < item.MapSize.Item2; y++)
                    {
                        Assert.AreEqual(item.Expected[x, y], map.FleeMap(x, y), "Identifier<" + item.Identifer + ">. X<" + x + ">. Y<" + y + ">.");
                    }
                }
            }
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void GetDirectionBoundsTest()
        {
            DijkstraMap map = new DijkstraMap(5, 5);
            map.GetDirection(new Location(6, 6));
        }

        [TestMethod()]
        public void GetDirectionTest()
        {
            var sampleMaps = new[]
            {
                new { Identifer = 0,
                      MapSize = new Tuple<int, int>(5, 5),
                      GoalCells = new bool[5, 5] { { false, false, false, false, false },
                                                   { false, false, false, false, false },
                                                   { false, false, true,  false, false },
                                                   { false, false, false, false, false },
                                                   { false, false, false, false, false } },
                      ImpassableCells = new bool[5, 5] { { false, false, false, false, false },
                                                         { false, false, false, false, false },
                                                         { false, false, false, false, false },
                                                         { false, false, false, false, false },
                                                         { false, false, false, false, false } },
                      GoalMap = new int[5, 5] { { 30, 25, 20, 25, 30 },
                                                { 25, 15, 10, 15, 25 },
                                                { 20, 10, 00, 10, 20 },
                                                { 25, 15, 10, 15, 25 },
                                                { 30, 25, 20, 25, 30 } },
                      FleeMap = new int[5, 5] { { -36, -30, -24, -30, -36 },
                                                { -30, -21, -15, -21, -30 },
                                                { -24, -15, -06, -15, -24 },
                                                { -30, -21, -15, -21, -30 },
                                                { -36, -30, -24, -30, -36 } } },
                new { Identifer = 1,
                      MapSize = new Tuple<int, int>(5, 5),
                      GoalCells = new bool[5, 5] { { false, false, false, false, false },
                                                   { false, false, false, false, false },
                                                   { false, false, true,  false, false },
                                                   { false, false, false, false, false },
                                                   { false, false, false, false, false } },
                      ImpassableCells = new bool[5, 5] { { false, false, false, false, false },
                                                         { false, true,  false, true,  false },
                                                         { false, true,  false, true,  false },
                                                         { false, true,  true,  true,  false },
                                                         { false, false, false, false, false } },
                      GoalMap = new int[5, 5] { { 35, 25,           20,           25,           35 },
                                                { 40, int.MaxValue, 10,           int.MaxValue, 40 },
                                                { 50, int.MaxValue, 00,           int.MaxValue, 50 },
                                                { 60, int.MaxValue, int.MaxValue, int.MaxValue, 60 },
                                                { 70, 75,           85,           75,           70 } },
                      FleeMap = new int[5, 5] { { -47, -42,          -32,          -42,          -47 },
                                                { -57, int.MaxValue, -27,          int.MaxValue, -57 },
                                                { -67, int.MaxValue, -17,          int.MaxValue, -67 },
                                                { -77, int.MaxValue, int.MaxValue, int.MaxValue, -77 },
                                                { -84, -92,          -102,         -92,          -84 } } },
            };

            var sampleData = new[]
            {
                //Map 0, Desire 1
                new { Map = 0, Location = new Location(0, 0), Desire = 1, Expected = Direction.SouthEast },
                new { Map = 0, Location = new Location(0, 1), Desire = 1, Expected = Direction.SouthEast },
                new { Map = 0, Location = new Location(0, 2), Desire = 1, Expected = Direction.East },
                new { Map = 0, Location = new Location(0, 3), Desire = 1, Expected = Direction.NorthEast },
                new { Map = 0, Location = new Location(0, 4), Desire = 1, Expected = Direction.NorthEast },
                new { Map = 0, Location = new Location(1, 0), Desire = 1, Expected = Direction.SouthEast },
                new { Map = 0, Location = new Location(1, 1), Desire = 1, Expected = Direction.SouthEast },
                new { Map = 0, Location = new Location(1, 2), Desire = 1, Expected = Direction.East },
                new { Map = 0, Location = new Location(1, 3), Desire = 1, Expected = Direction.NorthEast },
                new { Map = 0, Location = new Location(1, 4), Desire = 1, Expected = Direction.NorthEast },
                new { Map = 0, Location = new Location(2, 0), Desire = 1, Expected = Direction.South },
                new { Map = 0, Location = new Location(2, 1), Desire = 1, Expected = Direction.South },
                new { Map = 0, Location = new Location(2, 2), Desire = 1, Expected = Direction.Here },
                new { Map = 0, Location = new Location(2, 3), Desire = 1, Expected = Direction.North },
                new { Map = 0, Location = new Location(2, 4), Desire = 1, Expected = Direction.North },
                new { Map = 0, Location = new Location(3, 0), Desire = 1, Expected = Direction.SouthWest },
                new { Map = 0, Location = new Location(3, 1), Desire = 1, Expected = Direction.SouthWest },
                new { Map = 0, Location = new Location(3, 2), Desire = 1, Expected = Direction.West },
                new { Map = 0, Location = new Location(3, 3), Desire = 1, Expected = Direction.NorthWest },
                new { Map = 0, Location = new Location(3, 4), Desire = 1, Expected = Direction.NorthWest },
                new { Map = 0, Location = new Location(4, 0), Desire = 1, Expected = Direction.SouthWest },
                new { Map = 0, Location = new Location(4, 1), Desire = 1, Expected = Direction.SouthWest },
                new { Map = 0, Location = new Location(4, 2), Desire = 1, Expected = Direction.West },
                new { Map = 0, Location = new Location(4, 3), Desire = 1, Expected = Direction.NorthWest },
                new { Map = 0, Location = new Location(4, 4), Desire = 1, Expected = Direction.NorthWest },
                //Map 0, Desire -1
                new { Map = 0, Location = new Location(0, 0), Desire = -1, Expected = Direction.Here },
                new { Map = 0, Location = new Location(0, 1), Desire = -1, Expected = Direction.North },
                new { Map = 0, Location = new Location(0, 2), Desire = -1, Expected = Direction.North },
                new { Map = 0, Location = new Location(0, 3), Desire = -1, Expected = Direction.South },
                new { Map = 0, Location = new Location(0, 4), Desire = -1, Expected = Direction.Here },
                new { Map = 0, Location = new Location(1, 0), Desire = -1, Expected = Direction.West },
                new { Map = 0, Location = new Location(1, 1), Desire = -1, Expected = Direction.NorthWest },
                new { Map = 0, Location = new Location(1, 2), Desire = -1, Expected = Direction.NorthWest },
                new { Map = 0, Location = new Location(1, 3), Desire = -1, Expected = Direction.SouthWest },
                new { Map = 0, Location = new Location(1, 4), Desire = -1, Expected = Direction.West },
                new { Map = 0, Location = new Location(2, 0), Desire = -1, Expected = Direction.East },
                new { Map = 0, Location = new Location(2, 1), Desire = -1, Expected = Direction.NorthEast },
                new { Map = 0, Location = new Location(2, 2), Desire = -1, Expected = Direction.NorthWest },
                new { Map = 0, Location = new Location(2, 3), Desire = -1, Expected = Direction.SouthEast },
                new { Map = 0, Location = new Location(2, 4), Desire = -1, Expected = Direction.East },
                new { Map = 0, Location = new Location(3, 0), Desire = -1, Expected = Direction.East },
                new { Map = 0, Location = new Location(3, 1), Desire = -1, Expected = Direction.NorthEast },
                new { Map = 0, Location = new Location(3, 2), Desire = -1, Expected = Direction.NorthEast },
                new { Map = 0, Location = new Location(3, 3), Desire = -1, Expected = Direction.SouthEast },
                new { Map = 0, Location = new Location(3, 4), Desire = -1, Expected = Direction.East },
                new { Map = 0, Location = new Location(4, 0), Desire = -1, Expected = Direction.Here },
                new { Map = 0, Location = new Location(4, 1), Desire = -1, Expected = Direction.North },
                new { Map = 0, Location = new Location(4, 2), Desire = -1, Expected = Direction.North },
                new { Map = 0, Location = new Location(4, 3), Desire = -1, Expected = Direction.South },
                new { Map = 0, Location = new Location(4, 4), Desire = -1, Expected = Direction.Here },
                //Map 0, Desire 0
                new { Map = 0, Location = new Location(0, 0), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(0, 1), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(0, 2), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(0, 3), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(0, 4), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(1, 0), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(1, 1), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(1, 2), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(1, 3), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(1, 4), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(2, 0), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(2, 1), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(2, 2), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(2, 3), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(2, 4), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(3, 0), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(3, 1), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(3, 2), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(3, 3), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(3, 4), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(4, 0), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(4, 1), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(4, 2), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(4, 3), Desire = 0, Expected = Direction.Here },
                new { Map = 0, Location = new Location(4, 4), Desire = 0, Expected = Direction.Here },
                //Map 1, Desire 1
                new { Map = 1, Location = new Location(1, 1), Desire = 1, Expected = Direction.Here },
                new { Map = 1, Location = new Location(2, 2), Desire = 1, Expected = Direction.Here },
                new { Map = 1, Location = new Location(2, 4), Desire = 1, Expected = Direction.West },
                //Map 1, Desire -1
                new { Map = 1, Location = new Location(1, 1), Desire = -1, Expected = Direction.Here },
                new { Map = 1, Location = new Location(2, 2), Desire = -1, Expected = Direction.West },
                new { Map = 1, Location = new Location(2, 4), Desire = -1, Expected = Direction.East },
            };

            DijkstraMap[] dMaps = new DijkstraMap[sampleMaps.Length];

            for (int i = 0; i < sampleMaps.Length; i++)
            {
                dMaps[i] = new DijkstraMap(sampleMaps[i].MapSize.Item1, sampleMaps[i].MapSize.Item2);
                dMaps[i].BuildMap(sampleMaps[i].GoalCells, sampleMaps[i].ImpassableCells);
            }

            foreach (var item in sampleData)
            {
                Direction actual = dMaps[item.Map].GetDirection(item.Location, item.Desire);
                Assert.AreEqual(item.Expected, actual, "Map<" + item.Map + ">. Location<" + item.Location + ">. Desire<" + item.Desire + ">.");
            }
        }

        [TestMethod(), Timeout(50)]
        public void DijkstraPerformanceTest()
        {
            Tuple<int, int> MapSize = new Tuple<int, int>(50, 50);
            bool[,] GoalCells = new bool[50, 50];
            GoalCells.Init2D(false);
            GoalCells[0, 0] = true;

            bool[,] ImpassableCells = new bool[50, 50];
            ImpassableCells.Init2D(false);

            DijkstraMap mapOne = new DijkstraMap(MapSize.Item1, MapSize.Item2);
            mapOne.BuildMap(GoalCells, ImpassableCells);

            DijkstraMap mapTwo = new DijkstraMap(MapSize.Item1, MapSize.Item2);
            mapTwo.BuildMap(GoalCells, ImpassableCells);

            for (int x = 0; x < MapSize.Item1; x++)
            {
                for (int y = 0; y < MapSize.Item2; y++)
                {
                    Assert.AreEqual(mapOne.GoalMap(x, y), mapTwo.GoalMap(x, y), "GoalMap: X<" + x + ">. Y<" + y + ">.");
                    Assert.AreEqual(mapOne.FleeMap(x, y), mapTwo.FleeMap(x, y), "FleeMap: X<" + x + ">. Y<" + y + ">.");
                }
            }
        }
    }
}